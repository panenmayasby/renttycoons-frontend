var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');

gulp.task('sass', function(){
	gulp.src('./sass/custom.scss')
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(gulp.dest('./css/'))
		.pipe(livereload());
});

gulp.task('html', function(){
	gulp.src('./*.html', {read:false})
		.pipe(livereload());
});

gulp.task('php', function(){
	gulp.src('./*.php', {read:false})
		.pipe(livereload());
});

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('./*.html', ['html']);
	gulp.watch('./*.php', ['php']);
	gulp.watch('./sass/*.scss', ['sass']);
})

gulp.task('default', ['sass', 'html', 'php', 'watch']);
