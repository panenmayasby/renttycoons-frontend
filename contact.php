<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-contact">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Contact</li>
                    </ol>
                </div>

                <div class="main-content">
                    <div class="contact">
                        <div class="map_wrapper">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4473.854940140716!2d112.73522994180375!3d-7.345269573137006!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7f616606eb7c4bff!2sCarnival+Park+Suroboyo!5e0!3m2!1sen!2sid!4v1507173008066" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="row contact_bottom_wrapper">
                            <div class="col contact-us">
                                <div class="title">Contact Us</div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

                                    <p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat sed do eiusmod tempor incididunt ut labore dolore</p>
                                    <div class="contact-detail">
                                        <ul class="nav flex-column">
                                            <li>
                                                <span class="icon"><i class="pe-7s-map-marker pe-fw"></i></span>
                                                <span class="text">123 Suspendis mattis, Sollicitudin District, Accums Fringilla</span>
                                            </li>
                                            <li> 
                                                <span class="icon"><i class="pe-7s-mail pe-fw"></i></span>
                                                <span class="text">Email: support@domainstore.com</span>
                                            </li>
                                            <li>
                                                <span class="icon"><i class="pe-7s-call pe-fw"></i></span>
                                                <span class="text">Hotline: 0123456789</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col contact-comment">
                                <div class="title">Send Your Comments</div>
                                <div class="content">
                                    <form>
                                         <div class="form-group row">
                                            <div class="col-sm-8">
                                              <input type="text" class="form-control" id="inputPassword" placeholder="Your Name*">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-8">
                                              <input type="email" class="form-control" id="inputPassword" placeholder="Your Email*">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-8">
                                              <input type="text" class="form-control" id="inputPassword" placeholder="Your Phone*">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                              <textarea type="text" class="form-control" id="inputPassword" placeholder="Your Message*" rows="8"></textarea>
                                            </div>
                                        </div>
                                        <div class="button_wrapper">
                                            <a href="#" class="btn btn-submit">Send Message</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--END MAIN CONTENT-->
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>