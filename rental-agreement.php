<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-rentalagreement section-information">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Terms of Use</li>
                    </ol>
                </div>
                <div class="main-content main-information mb-5">
                    <div class="main-title">Terms of Use</div>
                    <div class="row information">
                        <div class="col-md-3 col-sm-4 nav-left_wrapper">
                            <nav class="nav flex-column nav-left">
                              <a class="nav-link" href="#">Terms of Use</a>
                              <a class="nav-link active" href="#">Sample Of Rental Agreement</a>
                              <a class="nav-link" href="#">Privacy Policy</a>
                              <a class="nav-link" href="#">Rental Policy</a>
                              <a class="nav-link" href="#">Prohibited Items</a>
                              <a class="nav-link" href="#">Media Releases</a>
                            </nav>
                        </div>
                        <div class="col-md-9 col-sm-8 info-right_wrapper">
                            <div class="sample-agreement-content">
                              <div class="title">This Rental Agreement is made and entered into as of the Effective Date noted below by and between:</div>
                              <div class="content">
                                <div class="agreement-between mt-5">
                                  <div class="row mb-2">
                                    <div class="col-left col-md-4 col-3">
                                      <hr>
                                    </div>
                                    <div class="col-right col-md-8 col-9">
                                       (the “Owner”) with ID <b><u>SXXXXXXXE</u></b> (IC), with a principal residence at 
                                    </div>
                                  </div>
                                  <div class="row mb-3">
                                    <div class="col-left col-md-4 col-3">
                                      <hr>
                                    </div>
                                    <div class="col-right col-md-8 col-9">
                                      and phone number <b><u>XXXXXXXX</u></b>  AND 
                                    </div>
                                  </div>
                                  <div class="row mb-2">
                                    <div class="col-left col-md-4 col-3">
                                      <hr>
                                    </div>
                                    <div class="col-right col-md-8 col-9">
                                       (the “Renter”) with ID <b><u>SXXXXXXXE</u></b> (IC), with a principal residence at 
                                    </div>
                                  </div>
                                  <div class="row mb-2">
                                    <div class="col-left col-md-4 col-3">
                                      <hr>
                                    </div>
                                    <div class="col-right col-md-8 col-9">
                                      and phone number <b><u>XXXXXXXX</u></b> 
                                    </div>
                                  </div>
                                </div>
                                <p>For good and valuable consideration, the parties agree as follows:</p>
    <p1>Rental.  Pursuant to the terms of this Agreement, the Owner shall rent certain Item(s), a description and condition of which are attached hereto as Exhibit A, (the “Item(s)”) to the Renter. The Owner has the right to terminate this contract if his or her item(s) is not available for rent. The Owner is liable to inform the Renter immediately if he or she needs to terminate the contract. The rental of the Items shall commence on Pick up Date (the “Start Date”) and end on Return Date (the “Termination Date”, and the period between the Start Date and Termination Date, the “Rental Period”). The Owner and the Renter shall arrange to have the Item(s) delivered to Renter by the Start Date and returned to the Owner by the Termination Date.</p1>
    <p1> Fees. </p1>
        <p2>(a) The Renter agrees to pay the Owner the following amounts:</p2>
            <p3>(i) Rate per time period (the “Rate”), or an aggregate amount (the “Aggregate Fee”), for rental of the Item(s).</p3>
            <p3>(ii) In the event that the Items (or any of them) are returned after the Termination Date at no fault of the Owner, the Renter shall pay the Owner daily rental charges for the number of day(s) exceeding the Termination Date.   </p3>
        <p2>(b) The Aggregate Fee shall become due and payable upon delivery of the Item(s)to the Renter. Late Return fees shall accrue on daily basis, shall become due upon the Termination Date and shall be payable at the end of each day following the Termination Date.  The Owner may, at his sole discretion, deduct the Late Return fees from the Deposit (as defined below). </p2>
    <p1>Security Deposit. The Renter shall pay to the Owner a security deposit of $[Listed in the Exhibit A] (the “Deposit”) on or before the Start Date.  This Deposit shall be reimbursed in full upon return of the Item to the Owner after any necessary deductions have been made pursuant to Section 4................</p1>
                            <p class="mt-4"><b>In witness Hereof</b>, the undersigned parties have electronically accepted and executed this Agreement as of the Effective Date:</br>Effective Date:   <b><u>YYYY-MM-DD</u></b></p>
                            <div class="sign text-center mt-4">
                              <div class="pull-left ml-3">
                                <p class="mb-5">
                                  The Renter
                                </p>
                                <p>
                                  <i>Renter's name</i>
                                </p>
                              </div>
                              <div class="pull-right mr-3">
                                 <p class="mb-5">
                                  The Owner
                                </p>
                                <p>
                                  <i>Owner's name</i>
                                </p>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                            <p class="mt-3 text-center">
                                <b>Exhibit A</br>
                                Item(s) to Be Rented</b>
                            </p>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script>
            $(document).ready(function() {
                
            });
            $(document).on('click', '.card-header', function(){ 
               $(this).parent().find(".card-body").slideToggle();
               $(this).parent().toggleClass('active');
           });
        </script>
    </body>
</html>