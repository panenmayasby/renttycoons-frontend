<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-message-chat">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link active" href="#">My Messages <span class="badge badge-secondary badge-orange">10</span></a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="link-back mt-2 mb-2">
                              <a href="#"><i class="pe-7s-back"></i> Back to Message</a>
                            </div>
                            <div class="table_wrappper">
                              <table class="table table-dashboard table-message-chat table-bordered">
                                <thead>
                                  <tr>
                                    <th>Item</th>
                                    <th>our2p-48</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td rowspan="2">
                                      <div class="media">
                                        <img class="d-flex align-self-center mr-3" src="images/message.jpg" alt="Generic placeholder image">
                                        <div class="media-body align-self-center">
                                          <p class="mb-0">Living Room Mat</p>
                                        </div>
                                      </div>
                                    </td>
                                    <td class="td-mainchat">
                                      <div class="chat_wrapper">
                                        <div class="date-chat">29 Jul</div>
                                         <div class="clearfix"></div>
                                        <div class="chat-row-grey chat-row">
                                          <div class="bubble-chat">Hi. Is the vacuum cleaner available for rental ?</div>
                                          <div class="time-chat">7:30 AM</div>
                                        </div>
                                        <div class="chat-row-green chat-row">
                                          <div class="bubble-chat">Hi Noraa,</br>
                                          It is available now, do let me know the starting date for rental.</div>
                                          <div class="time-chat">7:30 AM</div>
                                        </div>
                                        <div class="chat-row-grey chat-row">
                                          <div class="bubble-chat">Hi. Is the vacuum cleaner available for rental ?</div>
                                          <div class="time-chat">7:30 AM</div>
                                        </div>
                                        <div class="date-chat">30 Jul</div>
                                        <div class="clearfix"></div>
                                        <div class="chat-row-green chat-row">
                                          <div class="bubble-chat">Hi Noraa,</br>
                                          It is available now, do let me know the starting date for rental.</div>
                                          <div class="time-chat">7:30 AM</div>
                                        </div>
                                        <div class="chat-row-grey chat-row">
                                          <div class="bubble-chat">Hi. Is the vacuum cleaner available for rental ?</div>
                                          <div class="time-chat">7:30 AM</div>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="reply_wrapper">
                                      <form>  
                                        <div class="form-group">
                                          <textarea class="form-control" placeholder="Write Your Message Here" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                          <div class="text pull-left">** Your privacy is important, please do not share personal details.</div>
                                          <div class="button_wrapper pull-right">
                                            <a href="#" class="btn btn-square btn-bggreen">Send</a>
                                            <div class="clearfix"></div>
                                          </div>
                                        </div>
                                      </form>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>