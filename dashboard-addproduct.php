<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/uploader.css" />
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-addproduct">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard content-addproduct">
                            <div class="title">
                                Add Product
                            </div>
                            <div class="form_wrapper">
                              <form> 
                                <div class="form-group row">
                                  <label for="inputPassword" class="col-sm-3 col-form-label">Category</label>
                                  <div class="col-sm-9">
                                     <select id="inputState" class="form-control">
                                          <option disabled selected>Select Category</option>
                                          <option>Clothes</option>
                                          <option>Shoes</option>
                                      </select>
                                  </div>
                                </div><!--END FORM GROUP-->
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-3 col-form-label">Product Name</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="staticEmail">
                                  </div>
                                </div><!--END FORM GROUP-->
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-3 col-form-label">Tags (Comma Separated)</label>
                                  <div class="col-sm-9">
                                    <input type="text" class="form-control" id="staticEmail">
                                  </div>
                                </div><!--END FORM GROUP-->
                                <div class="form-group row">
                                  <label for="inputPassword" class="col-sm-3 col-form-label">Shipping</label>
                                  <div class="col-sm-9">
                                     <select id="inputState" class="form-control">
                                          <option disabled selected>Select Shipping Method</option>
                                          <option>Clothes</option>
                                          <option>Shoes</option>
                                      </select>
                                  </div>
                                </div><!--END FORM GROUP-->
                                 <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-3 col-form-label">Quantity</label>
                                  <div class="col-sm-2">
                                    <div class="input-group">
                                      <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"> <span class="input-group-addon">pcs</span>
                                    </div>
                                  </div>
                                </div><!--END FORM GROUP-->
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-3 col-form-label">Deposit Amount</label>
                                  <div class="col-sm-2">
                                    <div class="input-group">
                                      <span class="input-group-addon currency">S$</span>
                                      <input type="text" class="form-control noborder-left" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                  </div>
                                </div><!--END FORM GROUP-->
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-3 col-form-label">Price</label>
                                  <div class="col-sm-9">
                                    <div class="row">
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <span class="input-group-addon">Daily</span>
                                          <span class="input-group-addon currency">S$</span>
                                          <input type="text" class="form-control noborder-left" aria-label="Amount (to the nearest dollar)">
                                        </div>
                                      </div><!--END COL-->
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <span class="input-group-addon">Weekly</span>
                                          <span class="input-group-addon currency">S$</span>
                                          <input type="text" class="form-control noborder-left" aria-label="Amount (to the nearest dollar)">
                                        </div>
                                      </div><!--END COL-->
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <span class="input-group-addon">Monthly</span>
                                          <span class="input-group-addon currency">S$</span>
                                          <input type="text" class="form-control noborder-left" aria-label="Amount (to the nearest dollar)">
                                        </div>
                                      </div><!--END COL-->
                                    </div>
                                  </div>
                                </div><!--END FORM GROUP-->
                                <div class="form-group row group-upload">
                                  <label for="staticEmail" class="col-sm-3 col-form-label">Upload Image</label>
                                  <div class="col-sm-9">
                                    <div class="img-store-profile">
                                        <div id="wp-preview">
                                        </div>
                                        <div id="wp-uploader" class="upload-product">
                                          <button type="button" class="btn btn-sm btn-default btn-upload">
                                            <img src="images/uploadimages.png">
                                          </button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <span class="orange mr-1">*</span><span class="text">Your image size must be less than 1 MB. For iPad users, download Safari Upload Enabler to upload images</span>
                                  </div>
                                </div><!--END FORM GROUP-->
                                 <div class="form-group">
                                    <div class="pull-right">
                                      <a href="#" class="btn btn-square btn-bggreen">Save</a>
                                    </div>
                                </div><!--END FORM GROUP-->
                              </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/uploader.js"></script>
        <script>
        $(document).ready(function() {
            var fileUpload = $("#wp-uploader").uploader({
               filePreview : '#wp-preview',
               fileUpload  : '.upload-product',
               arrFileType : ["image/png", "image/jpeg"],
               maxTotalFiles : '5'
            });
          });
        </script>
    </body>
</html>