<footer>
	<div class="footer-top_wrapper">
		<div class="container footer-top">
			<div class="row no-gutters justify-content-md-center">
				<div class="left col-md-auto">
					<div class="community">JOIN OUR COMMUNITY & BE PART OF SHARING ECONOMY REVOLUTION</div>
					<div class="words">WHY BUY WHEN YOU CAN RENT</div>
					<div class="social">
						<a href="#" class="btn btn-social-round"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#" class="btn btn-social-round"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="#" class="btn btn-social-round"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<a href="#" class="btn btn-social-round"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="right col-md-auto">
					<div class="arrow-icon"><img src="images/icon/arrow.png"></div>
					<div class="btn_wrapper">
						<a href="#" class="btn btn-bggreen">Register</a>
					</div>
					<div class="text">Have we mentioned that it’s totally FREE?</div>
				</div>
			</div>

		</div>
	</div>
	<div class="footer-middle_wrapper">
		<div class="container footer-middle">
			<nav class="nav nav-justified">
  				<a class="nav-link active" href="#">Home</a>
  				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Blog</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">term  of use</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Prohibited items</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">rental policies</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Faq</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Media release</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">PDPA</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Collaboration</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">site map</a>
			</nav>
		</div>
	</div>
	<div class="footer-bottom_wrapper">
		<div class="container footer-bottom">
			<div class="left pull-left">RENT TYCOONS, Make Money, Save Money & Be Green ™</div>
			<div class="center text-center">Copyright © 2011 - 2017</div>
			<div class="right pull-right">BRN: 201111570D</div>
			<div class="clearfix"></div>
		</div>
	</div>
</footer>