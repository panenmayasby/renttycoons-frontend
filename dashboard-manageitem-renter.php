<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-manageitem-renter dashboard-manageitem">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="nav-manageitem mb-3">
                                <nav>
                                  <ul class="nav">
                                    <li class="nav-item">
                                      <a class="nav-link" href="#">Inventory</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="#">Rented to Others (I am an Owner)</a>
                                    </li>
                                    <li class="nav-item active">
                                      <a class="nav-link" href="#">Rented from Others (I am a Renter)</a>
                                    </li>
                                  </ul>
                                </nav>
                            </div>
                            <div class="pagination_wrapper mt-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                                <div class="status-filter col-md-4 ml-md-auto pr-0">
                                  Status : 
                                  <select id="inputState" class="sort-status">
                                      <option>All</option>
                                      <option>Accepted</option>
                                      <option>Rejected</option>
                                  </select>
                                </div>
                            </div>
                            <div class="manage-item">
                              <div class="item_wrapper">
                                <div class="row">
                                    <div class="col-md-8 left pr-2">
                                      <div class="media">
                                        <img class="d-flex mr-3 align-self-center" src="images/item.jpg" alt="Item Name">
                                        <div class="media-body">
                                          <div class="mt-0 product-name mb-2">Center-aligned media</div>
                                          <p class="mb-2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                          <p class="mb-3">
                                            e-Contract #2630 (Owner Name: <a href="#" class="user-name">rachelqiukexin-76</a>)
                                          </p>
                                          <p class="media-bottom">
                                            <span>S$ 5 / day</span>|<span>S$ 27 / week</span>|<span>S$ 100 / month</span>
                                          </p>
                                        </div>
                                      </div><!--END MEDIA-->
                                    </div><!--END COL-->
                                    <div class="col-md-4 right status_wrapper pl-2">
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Status
                                        </div>
                                        <div class="col-7">
                                          : <span class="green">Reserved</span>
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Rental Period
                                        </div>
                                        <div class="col-7">
                                          : 2017-09-05 - 2017-09-05
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Payment Method 
                                        </div>
                                        <div class="col-7">
                                          : Bank
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Rental Fee
                                        </div>
                                        <div class="col-7">
                                          : S$ 25
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Deposit Fee
                                        </div>
                                        <div class="col-7">
                                          : S$ 200
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Total
                                        </div>
                                        <div class="col-7">
                                          : S$ 225 (Before Discount)
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Coupon/Discount 
                                        </div>
                                        <div class="col-7">
                                          : NIL
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Total 
                                        </div>
                                        <div class="col-7">
                                          : S$ 225 (After Discount)
                                        </div>
                                      </div><!--END ROW-->
                                    </div><!--END COL-->
                                </div><!--END ROW-->
                                <div class="item-bottom mt-3">
                                    <nav class="nav">
                                      <a class="nav-link" href="#"><i class="pe-7s-mail pe-va"></i> Message Owner</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-news-paper pe-va"></i> View Contract</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-look pe-va"></i> View Item Page</a>
                                      <a class="nav-link" href="#" data-toggle="modal" data-target="#modal-paymentconfirmation"><i class="pe-7s-credit pe-va"></i> Confirm Bank Payment</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-close pe-va" style="font-size:22px;margin-right:0px;"></i> Cancel Contract</a>
                                    </nav>
                                </div>
                              </div><!--END ITEM-->
                               <div class="item_wrapper dashboard-empty">
                                 You don't have any items
                               </div>
                              <div class="item_wrapper">
                                <div class="row">
                                    <div class="col-md-8 left pr-2">
                                      <div class="media">
                                        <img class="d-flex mr-3 align-self-center" src="images/item.jpg" alt="Item Name">
                                        <div class="media-body">
                                          <div class="mt-0 product-name mb-2">Center-aligned media</div>
                                          <p class="mb-2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                          <p class="mb-0">
                                            Status: <a href="#" class="orange">Canceled</a>
                                          </p>
                                          <p class="mb-3">
                                            e-Contract #2630 (Owner Name: <a href="#" class="user-name">rachelqiukexin-76</a>)
                                          </p>
                                          <p class="media-bottom">
                                            <span>S$ 5 / day</span>|<span>S$ 27 / week</span>|<span>S$ 100 / month</span>
                                          </p>
                                        </div>
                                      </div><!--END MEDIA-->
                                    </div><!--END COL-->
                                    <div class="col-md-4 right status_wrapper pl-2 link-right align-self-center">
                                      <nav class="nav">
                                        <a class="nav-link" href="#"><i class="pe-7s-mail pe-va"></i> Message Owner</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-look pe-va"></i> View Item Page</a>
                                        <a class="nav-link orange" href="#"><i class="pe-7s-news-paper pe-va"></i> e-Contract Canceled</a>
                                      </nav>
                                    </div><!--END COL-->
                                </div><!--END ROW-->
                              </div><!--END ITEM-->
                            </div><!--END MANAGE ITEM-->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>
        
         <!-- Modal PAYMENT CONFIRMATION -->
        <div class="modal fade modal-upgrade modal-paymentconfirmation" id="modal-paymentconfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Payment Confirmation</div>
                <div class="content">
                  <div class="form_wrapper form_payment">
                    <form>
                      <div class="row_form">
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Date of Transfer:</label>
                          <div class="col-sm-8 date_wrapper">
                            <input type="text" class="form-control date-picker date-transfer" id="date-transfer">
                            <i class="pe-7s-browser"></i>
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label pt-0 pr-0">Internet Bank Transfer Reference Number:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="staticEmail" value="">
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Transferred to:</label>
                          <div class="col-sm-8">
                            <select id="inputState" class="form-control">
                                  <option disabled selected>Select Bank</option>
                                  <option>DBS</option>
                                  <option>OCBC</option>
                              </select>
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Transaction Amount (SGD):</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="staticEmail" value="">
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Remarks:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="staticEmail" placeholder="">
                          </div>
                        </div><!--END FORM GROUP-->
                    </form>
                  </div>
                  <div class="button_wrapper mt-4">
                    <a href="#" class="btn btn-bggreen btn-block btn-square">PAYMENT CONFIRMATION</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--END MODAL-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/moment.js"></script>
        <script type="text/javascript" src="js/daterangepicker.js"></script>
        <script>
            $('.date-transfer').daterangepicker({
                "singleDatePicker": true,
                "startDate": "10/12/2017",
                "endDate": "09/29/2017"
            }, function(start, end, label) {
              console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
            });
        </script>

    </body>
</html>