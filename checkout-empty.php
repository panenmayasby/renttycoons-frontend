<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-checkout">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item"><a href="#">Bedroom</a></li>
                      <li class="breadcrumb-item active">Checkout</li>
                    </ol>
                </div>
                <div class="main-content main-checkout">
                    <div class="main-title mb-3">Checkout</div>
                    <div class="step1_wrapper step_wrapper mb-4">
                        <div class="card">
                          <div class="card-header">
                            Step 1: Your Rental Items
                          </div>
                          <div class="card-body">
                            <div class="table_wrapper">
                                <table class="table table-checkout table-dashboard table-bordered align-middle">
                                  <thead>
                                    <tr>
                                      <th>
                                        <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </th>
                                      <th class="text-center">Product</th>
                                      <th class="text-center pl-1 pr-1">Quality</th>
                                      <th class="text-center">Rental Rate</th>
                                      <th class="text-center">Rental Period</th>
                                      <th class="text-center pl-2 pr-2">Rental Cost</th>
                                      <th class="text-center pl-2 pr-2">Deposit</th>
                                      <th class="text-center pl-2 pr-2">Delivery</th>
                                    </tr>
                                  </thead>
                                 <tbody>
                                  <tr>
                                      <td colspan="8" class="row-empty">
                                        Your cart is empty
                                      </td>
                                    </tr>
                                 </tbody>
                              </table>
                            </div>
                            <div class="coupon-disc_wrapper">
                                <label class="green">Coupon discount</label>
                                <div class="row no-gutters">
                                   <div class="input-group col-md-3">
                                      <input type="text" class="form-control" placeholder="" aria-label="Search for...">
                                      <span class="input-group-btn">
                                        <a class="btn btn-search"><i class="pe-7s-search"></i></a>
                                      </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="table_wrapper">
                                <table class="table table-total table-dashboard align-middle">
                                    <thead>
                                        <tr>
                                            <th>Total Rent Cost</th>
                                            <th>Discount</th>
                                            <th>Deposit</th>
                                            <th>Delivery Cost</th>
                                            <th>Total Payable</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>S$ 75</td>
                                            <td>S$ 0</td>
                                            <td>S$ 90</td>
                                            <td>S$ 6</td>
                                            <td>S$ 171</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="button_wrapper mt-4">
                                <div class="pull-left text mt-2">
                                    <div class="form-check">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            I have read and understood the&nbsp; <a href="#" class="green">Terms of Use</a>.
                                        </label>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-checkout mr-3">Continue Browsing</a>
                                    <a href="#" class="btn btn-checkout">Checkout</a>
                                </div>
                              <div class="clearfix"></div>
                            </div>
                            <div class="text mt-4">
                                * Contact details of the owner <a href="#">rachelqiukexin-76</a> will be given after you complete your payment.</br>
                                * Your security deposit will be refunded by the owner <a href="#">rachelqiukexin-76</a> after you return the item.
                            </div>
                          </div>
                        </div>
                    </div><!--END STEP 1-->
                    <div class="step2_wrapper step_wrapper mb-4">
                        <div class="card">
                          <div class="card-header">
                            Step 2: Payment Method
                          </div>
                          <div class="card-body">
                            <div class="total-payment mb-3">
                                Total payment: S$ 115.00
                            </div>
                            <div class="payment-option pl-2 pr-2">
                                Please choose your payment option here:
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <form> 
                                            <div class="form-group row row_form no-gutters">
                                                <div class="col-auto radio-custom_wrapper pt-0 pb-0">
                                                    <label class="custom-control custom-radio">
                                                      <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                      <span class="custom-control-indicator"></span>
                                                    </label>
                                                </div>
                                                <div class="col-10 custom-control-description">
                                                    <label for="staticEmail" class="col-form-label pt-1 mb-2">Bank Transfer:</label>
                                                    <select id="inputState" class="form-control">
                                                        <option disabled selected>Select Bank</option>
                                                        <option>DBS</option>
                                                        <option>OCBC</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row row_form no-gutters mt-4">
                                                <div class="col-auto radio-custom_wrapper pt-0 pb-0">
                                                    <label class="custom-control custom-radio">
                                                      <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                      <span class="custom-control-indicator"></span>
                                                    </label>
                                                </div>
                                                <div class="col-10 custom-control-description">
                                                    <label for="staticEmail" class="col-form-label pt-1 mb-2">Paypal</label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6 border-left align-self-center">
                                        Account Name : Rent Tycoons Pte. Ltd.</br>
                                        Account Number : 003-943683-1 (Acct.Type:Current)</br>
                                        Bank Code : 7171</br>
                                        Branch Code : 003
                                    </div>
                                </div>
                            </div>
                            <div class="button_wrapper mt-4">
                                <div class="row">
                                    <div class="col-md-10 text">
                                        <span class="orange">*</span> Your reservation is successful and will expire within 6 hours. Kindly make your payment promptly in order to secure your item(s).
   If you accidently close the page, look for your email [Rent Tycoons] Please Make Payment for the link to confirm your payment!
   If you want to re-do your reservation, you can go to Rented From Others and click on Cancel Contract.
                                    </div>
                                    <div class="col-md-2">                                         
                                      <a href="#" class="btn btn-square btn-bggreen pull-right">Continue</a>
                                    </div>
                                </div>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                    </div><!--END STEP 2-->
                    <div class="step3_wrapper step_wrapper mb-4">
                        <div class="card">
                          <div class="card-header">
                            Step 3: Payment Confirmation
                          </div>
                          <div class="card-body">
                              <form> 
                                <div class="row row_form">
                                    <div class="col-md-6">
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Date of Transfer:</label>
                                        <div class="col-sm-8 date_wrapper">
                                          <input type="text" class="form-control date-picker" id="date-transfer">
                                          <i class="pe-7s-browser"></i>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label pt-0">Internet Bank Transfer Reference Number:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" value="">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Remarks:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Transferred to:</label>
                                        <div class="col-sm-8">
                                          <select id="inputState" class="form-control">
                                                <option disabled selected>Select Bank</option>
                                                <option>DBS</option>
                                                <option>OCBC</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Transaction Amount (SGD):</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" value="">
                                        </div>
                                      </div>
                                    </div>                           
                                </div>
                                <div class="button_wrapper mt-4">
                                    <div class="pull-left text">
                                        <span class="orange">*</span> Email: contact@RentTycoons.com if you have questions regarding payment.
                                    </div>
                                  <a href="#" class="btn btn-square btn-bggreen pull-right">Save</a>
                                  <div class="clearfix"></div>
                                </div>
                              </form>
                          </div>
                        </div>
                    </div><!--END STEP 3-->
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/moment.js"></script>
        <script type="text/javascript" src="js/daterangepicker.js"></script>
        <script>
            $('#date-transfer').daterangepicker({
                "singleDatePicker": true,
                "startDate": "10/12/2017",
                "endDate": "09/29/2017"
            }, function(start, end, label) {
              console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
            });
            $('.date-pickerrent').daterangepicker({
                "singleDatePicker": true,
                "startDate": "10/12/2017",
                "endDate": "09/29/2017"
            }, function(start, end, label) {
              console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
            });
        </script>
    </body>
</html>