<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-home">
            <div class="container">
                <div class="main-tab mb-3">
                    <div class="row no-gutters">
                        <div class="col-3 sidebar_wrapper">
                            <div class="nav flex-column nav-tophome sidebar" id="v-pills-tab" role="tablist">
                              <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-expanded="true"><div class="border-bottom">Cooker</div></a>
                              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-expanded="true"><div class="border-bottom">Cleaning</div></a>
                              <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-expanded="true"><div class="border-bottom">Kitchen Tools</div></a>
                              <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true"><div class="border-bottom">Bowls</div></a>
                              <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true"><div class="border-bottom">Refrigrators</div></a>

                              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-expanded="true"><div class="border-bottom">Cleaning</div></a>
                              <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-expanded="true"><div class="border-bottom">Kitchen Tools</div></a>
                              <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true"><div class="border-bottom">Bowls</div></a>
                              <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true"><div class="border-bottom">Refrigrators</div></a>
                            </div>
                        </div>
                        <div class="col-9"> 
                            <div class="tab-content" id="v-pills-tabContent">
                              <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                  <div class="row no-gutters">
                                    <div class="col-md-8 col-sm-8">
                                        <a href="#">
                                            <img src="images/banner-home1.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <a href="#">
                                            <img src="images/banner-home2.jpg" class="img-fluid">
                                        </a>
                                        <a href="#">
                                            <img src="images/banner-home3.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                  </div>
                              </div>
                              <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                  <div class="row no-gutters">
                                    <div class="col-md-8 col-sm-8">
                                        <a href="#">
                                            <img src="images/banner-home1.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <a href="#">
                                            <img src="images/banner-home2.jpg" class="img-fluid">
                                        </a>
                                        <a href="#">
                                            <img src="images/banner-home3.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                  </div>
                              </div>
                              <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                  <div class="row no-gutters">
                                    <div class="col-md-8 col-sm-8">
                                        <a href="#">
                                            <img src="images/banner-home1.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <a href="#">
                                            <img src="images/banner-home2.jpg" class="img-fluid">
                                        </a>
                                        <a href="#">
                                            <img src="images/banner-home3.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                  </div>
                              </div>
                              <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                  <div class="row no-gutters">
                                    <div class="col-md-8 col-sm-8">
                                        <a href="#">
                                            <img src="images/banner-home1.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <a href="#">
                                            <img src="images/banner-home2.jpg" class="img-fluid">
                                        </a>
                                        <a href="#">
                                            <img src="images/banner-home3.jpg" class="img-fluid">
                                        </a>
                                    </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div><!--END MAIN TAB-->
                <div class="home-promotion">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="title-home mb-3">
                               TOP RATED
                               <div class="owl-nav">
                                    <a class="owl-prevpromotion btn btn-owlnav mr-1">
                                        <i class="pe-7s-angle-left"></i>
                                    </a>
                                    <a class="owl-nextpromotion btn btn-owlnav ">
                                        <i class="pe-7s-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="owl-carousel owl-theme owl-promotion">
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                    <div class="img-product_wrapper mr-3">
                                                        <img src="images/tes.jpg" alt="Generic placeholder image">
                                                      </div>

                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <div class="img-product_wrapper mr-3">
                                                        <img src="images/news-banner.jpg" alt="Generic placeholder image">
                                                      </div>

                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Ut enim ad minima
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>                                    
                                        </div><!--END ROW-->
                                    </div><!--END ITEM-->
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                       <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Ut enim ad minima
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>                                    
                                        </div><!--END ROW-->
                                    </div><!--END ITEM-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="title-home mb-3">
                               RECENTLY RENTED
                               <div class="owl-nav">
                                    <a class="owl-prevrecent btn btn-owlnav mr-1">
                                        <i class="pe-7s-angle-left"></i>
                                    </a>
                                    <a class="owl-nextrecent btn btn-owlnav ">
                                        <i class="pe-7s-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="owl-carousel owl-theme owl-recent">
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <div class="img-product_wrapper mr-3">
                                                      <img src="images/tes.jpg" alt="Generic placeholder image">
                                                    </div>
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                         <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                         <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                         <a href="#">Automatic Espresso</a>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Ut enim ad minima
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>                                    
                                        </div><!--END ROW-->
                                    </div><!--END ITEM-->
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>
                                            <div class="col-md-6 col-sm-6 pr-0">
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Automatic Espresso
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                                <div class="media product-box-top">
                                                  <img class="d-flex align-self-center mr-3" src="images/home1.jpg" alt="Generic placeholder image">
                                                  <div class="media-body">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-name mt-2 mb-2">
                                                        Ut enim ad minima
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                  </div>
                                                </div><!--END MEDIA-->
                                            </div>                                    
                                        </div><!--END ROW-->
                                    </div><!--END ITEM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="browse-product_wrapper">
                    <div class="title-home">
                        BROWSE PRODUCT
                    </div>
                    <div class="content">
                        <div class="row ml-0 mr-0">
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/tes.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                         <a href="#">Automatic Espresso</a>
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/news-banner.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-browse text-center">
                                <div class="img_wrapper">
                                    <img src="images/home2.jpg" class="img-fluid">
                                </div>
                                <div class="browse-desc mt-2">
                                    <div class="product-name">
                                        Automatic Espresso
                                    </div>
                                    <div class="product-price mt-1">
                                        $75.00
                                    </div>
                                    <div class="product-rating">
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star active"></i>
                                        <i class="pe-7s-star"></i>
                                        <i class="pe-7s-star"></i>
                                    </div>
                                </div>
                                <div class="btn-addcart_wrapper">
                                    <a href="#" class="btn btn-bggreen btn-square"><i class="pe-7s-cart"></i>&nbsp;ADD TO CART</a>
                                </div>
                                <div class="browse-hover">
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-like pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-copy-file pe-va"></i></a>
                                    <a href="#" class="btn btn-bgwhite btn-square mr-1"><i class="pe-7s-look pe-va"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="text-center see-all_wrapper mt-2 mb-3">
                            <a href="#" class="btn btn-see-all btn-square">See All Product</a>
                        </div>
                    </div>
                </div>
                <div class="blog_wrapper">
                    <div class="title-home mb-3">
                       OUR BLOG
                       <div class="owl-nav">
                            <a class="owl-prevblog btn btn-owlnav mr-1">
                                <i class="pe-7s-angle-left"></i>
                            </a>
                            <a class="owl-nextblog btn btn-owlnav ">
                                <i class="pe-7s-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="content">
                        <div class="owl-carousel owl-theme owl-blog">
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/homeblog.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/homeblog.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/homeblog.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/tes.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/homeblog.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/homeblog.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                            <div class="item col-blog">
                                <div class="img_wrapper">
                                    <img src="images/homeblog.jpg" class="img-fluid">
                                    <div class="date-blog">
                                        16</br>
                                        Aug
                                    </div>
                                </div>
                                <div class="blog-minicontent">
                                    <div class="title">Aliquam vitae dapibus ut</div>
                                    <div class="summary">
                                        Donec vitae hendrerit arcu, sit amet faucibus nisl. 
                                        Cras pretium arcu ex...
                                    </div>
                                    <div class="link pull-right">
                                        <a href="#">Read More</a>
                                    </div>
                                </div>
                            </div><!--END COL BLOG-->
                        </div><!--END OWL BLOG-->
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script>
             $(document).ready(function(){
                var owlblog = $('.owl-blog');
                owlblog.owlCarousel({
                    loop:true,
                    margin:30,
                    nav:false,
                    dots:false,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:2
                        },
                        1000:{
                            items:3
                        }
                    }
                });
                
                $('.owl-nextblog').on('click', function(){
                    owlblog.trigger('next.owl.carousel');
                });
                $('.owl-prevblog').on('click', function(){
                    owlblog.trigger('prev.owl.carousel');
                });

                var owlpromotion = $('.owl-promotion');
                owlpromotion.owlCarousel({
                    loop:true,
                    margin:30,
                    nav:false,
                    dots:false,
                    singleItems:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                });
                $('.owl-nextpromotion').on('click', function(){
                    owlpromotion.trigger('next.owl.carousel');
                });
                $('.owl-prevpromotion').on('click', function(){
                    owlpromotion.trigger('prev.owl.carousel');
                });

                var owlrecent = $('.owl-recent');
                owlrecent.owlCarousel({
                    loop:true,
                    margin:30,
                    nav:false,
                    dots:false,
                    singleItems:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                });
                $('.owl-nextrecent').on('click', function(){
                    owlrecent.trigger('next.owl.carousel');
                });
                $('.owl-prevrecent').on('click', function(){
                    owlrecent.trigger('prev.owl.carousel');
                });
            });
        </script>
    </body>
</html>