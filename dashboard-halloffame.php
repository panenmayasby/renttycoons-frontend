<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-halloffame">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                                My Dashboard
                            </div>
                            <div class="halloffame-box income-by-item mt-3 mb-5">
                                <div class="title">
                                    My Income By Item
                                </div>
                                <div class="table_wrapper">
                                    <table class="table table-income table-dashboard">
                                        <thead>
                                            <th>Item</th>
                                            <th>Rating</th>
                                            <th>Income</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Portable Speakers Amplifier (8hr battery with USB slot) - Usable with ipod/mp3 player (1 of 3 sets)</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                                <td>S$2200</td>
                                            </tr>
                                            <tr>
                                                <td>Delphin Vacuum Cleaner</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                                <td>S$2200</td>
                                            </tr>
                                            <tr>
                                                <td>Delphin Vacuum Cleaner</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                                <td>S$2200</td>
                                            </tr>
                                            <tr>
                                                <td>Delphin Vacuum Cleaner</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                                <td>S$2200</td>
                                            </tr>
                                            <tr>
                                                <td>Delphin Vacuum Cleaner</td>
                                                <td>
                                                    No Rating Yet
                                                </td>
                                                <td>S$2200</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    Total Income
                                                </td>
                                                <td class="text-center">
                                                    S$ 1.220.000
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                            <div class="halloffame-box income-by-item mt-3 mb-5">
                                <div class="title">
                                    What Renter(s) Say About Me
                                </div>
                                <div class="table_wrapper">
                                    <table class="table table-rating table-dashboard">
                                        <thead>
                                            <th>Renter</th>
                                            <th>Feedback</th>
                                            <th>Rating</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="#">nilesh13-11</a></td>
                                                <td>“Good experience. Prompt, friendly and timely. Would recommend.”</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">nilesh13-11</a></td>
                                                <td>“Good experience. Prompt, friendly and timely. Would recommend.”</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">nilesh13-11</a></td>
                                                <td>“Good experience. Prompt, friendly and timely. Would recommend.”</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">nilesh13-11</a></td>
                                                <td>“Good experience. Prompt, friendly and timely. Would recommend.”</td>
                                                <td>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    Average Rating
                                                </td>
                                                <td class="text-center">
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                             <div class="halloffame-box chart-rating_wrapper mt-3 mb-5">
                                <div class="title">
                                   My Rating
                                </div>
                                <div class="chart_wrapper mt-2">
                                    <p class="green">
                                        <b>Your profile has 3 ratings</b>
                                    </p>
                                    <div class="main-chart">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <canvas id="chart-profile"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/chart.bundle.js"></script>
        <script>
            var barChartData = {
                labels: ["Responsiveness", "Timeless", "Courteous", "Realibility"],
                datasets: [{
                    label: 'Rate',
                    backgroundColor: '#39b54a',
                    borderColor: '#cccccc',
                    borderWidth: 1,
                    data: [5,5,5,4.3 ]
                }]

            };

            window.onload = function() {
                var ctx = document.getElementById("chart-profile").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        responsive: true,
                        legend: {
                            position: 'right',
                        },
                        title: {
                            display: true,
                            text: 'Profile Rating',
                        }
                    }
                });

            };
        </script>
    </body>
</html>