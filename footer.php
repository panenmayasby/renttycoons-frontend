<footer>
	<div class="footer-middle_wrapper">
		<div class="container footer-middle">
			<nav class="nav nav-justified">
  				<a class="nav-link active" href="#">Home</a>
  				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Blog</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">term  of use</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Prohibited items</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">rental policies</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Faq</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Media release</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">PDPA</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">Collaboration</a>
				<a class="nav-link disabled">|</a>
				<a class="nav-link" href="#">site map</a>
			</nav>
		</div>
	</div>
	<div class="footer-bottom_wrapper">
		<div class="container footer-bottom">
			<div class="left pull-left">RENT TYCOONS, Make Money, Save Money & Be Green ™</div>
			<div class="center text-center">Copyright © 2011 - 2017</div>
			<div class="right pull-right">BRN: 201111570D</div>
			<div class="clearfix"></div>
		</div>
	</div>
</footer>