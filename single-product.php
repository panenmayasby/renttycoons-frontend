<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-singleproduct">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item"><a href="#">Bedroom</a></li>
                      <li class="breadcrumb-item active">Printed Dress</li>
                    </ol>
                </div>

                <div class="main-content">
                    <div class="row detail-product">
                        <div class="sidebar_wrapper col-md-3">
                            <div class="sidebar">
                                <nav class="nav flex-column">
                                  <a class="nav-link active" href="#">COOKER</a>
                                  <a class="nav-link" href="#">Cleaning</a>
                                  <a class="nav-link" href="#">Kitchen Tools</a>
                                  <a class="nav-link" href="#">Bowls</a>
                                  <a class="nav-link" href="#">Refrigrator</a>
                                  <a class="nav-link" href="#">Blender</a>
                                </nav>
                            </div>
                        </div><!--END SIDEBAR-->
                        <div class="single_product_right col-md-9">
                            <div class="row">
                                <div class="mainimg-product_wrapper col-md-5">
                                    <div class="img-top">
                                        <img src="images/product1.jpg" class="img-fluid">
                                    </div>
                                    <div class="owl-top_wrapper mt-3">
                                        <div class="owl-carousel owl-theme owl-mainproduct">
                                            <div class="item img-product_wrapper"><img src="images/tes.jpg"></div>
                                            <div class="item img-product_wrapper"><img src="images/productsmall2.jpg"></div>
                                        </div>
                                        <a class="owl-mainprev btn btn-owlnav pull-left">
                                            <i class="pe-7s-angle-left"></i>
                                        </a>
                                        <a class="owl-mainnext btn btn-owlnav pull-right">
                                            <i class="pe-7s-angle-right"></i>
                                        </a>
                                    </div>
                                    <div class="violate_wrapper">
                                       <i class="pe-7s-attention"></i> &nbsp; This item is violating Term of Use,  <a href="#">Report This !</a>
                                    </div>
                                </div>
                                <div class="mainproduct-detail_wrapper col-md-7">
                                    <div class="product-name product-detail-row">
                                        Printed Dress
                                    </div>
                                    <div class="product-rating-review product-detail-row">
                                        <div class="product-rating">
                                            <i class="pe-7s-star active"></i>
                                            <i class="pe-7s-star active"></i>
                                            <i class="pe-7s-star active"></i>
                                            <i class="pe-7s-star"></i>
                                            <i class="pe-7s-star"></i>
                                        </div>
                                        <span class="review">0 reviews</span>
                                    </div><!--END RATING-->
                                    <div class="product-location border-bottom product-detail-row">
                                        <div class="location-row">
                                            Location:&nbsp;<span class="green">540202</span>
                                        </div>
                                        <div class="location-row">
                                            MRT Station:&nbsp;<span class="green">Jurong East</span>
                                        </div>
                                        <div class="location-row">
                                            Refundable Deposit:&nbsp;<span class="green">S$10</span>
                                        </div>
                                    </div><!--END LOCATION-->
                                    <div class="product-price border-bottom product-detail-row">
                                        <div class="row">
                                            <div class="col align-self-center col-price">Price:</div>
                                            <div class="col text-center">
                                                <div class="small">Daily</div>
                                                <div class="big">-</div>
                                            </div>
                                            <div class="col text-center">
                                                <div class="small">Weekly</div>
                                                <div class="big">S$10</div>
                                            </div>
                                            <div class="col text-center">
                                                <div class="small">Monthly</div>
                                                <div class="big">-</div>
                                            </div>
                                            <div class="col">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-status product-detail-row border-bottom">
                                        <div class="row">
                                            <div class="col-5 border-right align-self-center"> 
                                                Item Status: <span class="green">Available</span>
                                            </div>
                                            <div class="col-7">
                                                Quantity: <span class="green">2</span>
                                                <div class="button_wrapper">
                                                    <a href="#" class="btn btn-bggreen btn-square">Ask Owner</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END PRICE-->
                                    <div class="tycoonrating product-detail-row border-bottom">
                                        <div class="row">
                                            <div class="col-auto">
                                                Tycoon's rating:</br>
                                                <a href="#">johndoe25</a>
                                            </div>
                                            <div class="col-auto align-self-center">
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END TYCOON RATING-->
                                    <div class="delivery-method product-detail-row radio-custom_wrapper align-self-center border-bottom">
                                        <label class="custom-control custom-radio">
                                          <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                          <span class="custom-control-indicator"></span>
                                          <span class="custom-control-description">Self Collect</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                          <input id="radio2" name="radio" type="radio" class="custom-control-input">
                                          <span class="custom-control-indicator"></span>
                                          <span class="custom-control-description">Delivery</span>
                                        </label>
                                        <div class="price-delivery">S$5 </div>
                                    </div><!--END DELIVERY METHOD-->
                                    <div class="quantity_wrapper product-detail-row border-bottom">
                                        <div class="row">
                                          <div class="col-lg-5 col-md-5">
                                            <div class="input-group">
                                              <span class="input-group-btn mr-2">
                                                <a class="btn btn-main"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                              </span>
                                              <input type="text" class="form-control text-center mr-2" aria-label="Product name" value="1">
                                              <span class="input-group-btn">
                                                <a class="btn btn-main"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                              </span>
                                            </div>
                                          </div>
                                          <div class="col-lg-7 col-md-7">
                                            <a href="#" class="btn btn-bggreen btn-square">Rent Now</a>
                                          </div>
                                        </div>
                                    </div> <!--END QUANTITY-->
                                    <div class="all-items product-detail-row border-bottom">
                                        <a href="#"><i class="pe-7s-search pe-va"></i> See All Items by johndoe25</a>
                                    </div>
                                    <div class="share_wrapper align-self-center">
                                        <div class="share pull-left">
                                            <span>Share This</span>
                                            <div class="social align-self-center">
                                              <a href="#" class="btn"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                              <a href="#" class="btn"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                              <a href="#" class="btn"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                              <a href="#" class="btn"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-detail-tabs mb-4">
                        <nav class="nav nav-tabs" id="myTab" role="tablist">
                          <a class="nav-item nav-link single-title active" id="nav-desc-tab" data-toggle="tab" href="#nav-desc" role="tab" aria-controls="nav-desc" aria-expanded="true"><div>Description</div></a>
                          <a class="nav-item nav-link single-title" id="nav-pickup-tab" data-toggle="tab" href="#nav-pickup" role="tab" aria-controls="nav-pickup"><div>PICKUP & RETURN INSTRUCTION</div></a>
                          <a class="nav-item nav-link single-title" id="nav-reviews-tab" data-toggle="tab" href="#nav-reviews" role="tab" aria-controls="nav-reviews"><div>Reviews</div></a>
                          <a class="nav-item nav-link single-title" id="nav-message-tab" data-toggle="tab" href="#nav-message" role="tab" aria-controls="nav-message"><div>MESSAGE OWNER</div></a>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                          <div class="tab-pane fade show active" id="nav-desc" role="tabpanel" aria-labelledby="nav-desc-tab">
                            <div class="text mb-2">
                                <p>Shank porchetta anim ham in esse. Duis swine mollit incididunt, quis pork belly rump ut ball tip venison strip steak pancetta proident. Ground round duis beef, eu sunt non dolor esse capicola shoulder strip steak. Ut fatback chuck minim exercitation kielbasa. Cupidatat nostrud prosciutto corned beef meatball sausage lorem.</p>

                                <p>Filet mignon corned beef laboris ipsum porchetta beef irure dolor fatback. Pariatur cupim occaecat short ribs consectetur, venison ipsum flank hamburger bacon tri-tip. Eu duis est cupidatat prosciutto alcatra, consectetur aute velit shoulder shankle ham hock in. Cillum deserunt velit dolor, beef ribs excepteur hamburger flank occaecat. Qui dolor flank picanha t-bone lorem filet mignon magna anim laboris pancetta short ribs nostrud reprehenderit. Biltong cillum ex est, in consequat jerky ea ad in tri-tip frankfurter consectetur pancetta deserunt. Beef ribs chicken filet mignon esse leberkas pancetta aliquip turkey cupidatat id sed bacon mollit.</p>

                                <p>Ground round jowl pig, short ribs turducken cillum labore aliqua bacon ea doner anim esse. Jerky laboris id, fatback ut velit est laborum ham hock veniam shoulder exercitation lorem. Dolore eu landjaeger cupidatat venison occaecat adipisicing swine. Proident turducken aliquip ham hock minim non meatball do. Nisi andouille pancetta, doner laborum mollit adipisicing kielbasa et.</p>
                            </div>
                            <div class="tags">
                                <label>Tags:</label>
                                <div class="tags-list">
                                    <a href="#" class="btn btn-tags">Furniture</a>
                                    <a href="#" class="btn btn-tags">Self</a>
                                    <a href="#" class="btn btn-tags">Wood</a>
                                </div>
                            </div>
                            <div class="note">
                                <b>Note:</b> To re-do your reservation, go to <a href="#">Rented From Others</a> and click on Cancel Contract
                            </div>
                          </div>
                          <div class="tab-pane fade" id="nav-pickup" role="tabpanel" aria-labelledby="nav-pickup-tab">
                            <div class="text">
                                <p>Siglap Road next to Villa Marina; near Mandarin Gardens </p>

                                <p>Junction of Marine Parade Road and Siglap Road</br>
                                    Front gate is next to Villa Marina</p>

                                    <p>Buses along Marine Parade Road: 31 36 43 48 55 135 155 196 197</br>
                                    Buses along East Coast Road (near Siglap Centre): 10 12 14 40 155</br>
                                    Buses along Siglap Road (near Sekolah Indonesia): 13 16 155</p>
                            </div>
                          </div>
                          <div class="tab-pane fade" id="nav-reviews" role="tabpanel" aria-labelledby="nav-reviews-tab">
                            <div class="reviews_wrapper">
                                <ul class="list-unstyled">
                                  <li class="media">
                                      <div class="media-body">
                                            <div class="pull-left nopadding">
                                                <h4 class="media-heading user-name">Ian</h4>
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                </div>
                                            </div>
                                            <div class="pull-right message-time">
                                              08 November 2016
                                            </div>
                                              <div class="clearfix"></div>
                                              <p class="mt-2">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                                      </div><!-- end media -->
                                  </li>
                                  <li class="media">
                                      <div class="media-body">
                                            <div class="pull-left nopadding">
                                                <h4 class="media-heading user-name">Ian</h4>
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                </div>
                                            </div>
                                            <div class="pull-right message-time">
                                              08 November 2016
                                            </div>
                                              <div class="clearfix"></div>
                                              <p class="mt-2">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                                      </div><!-- end media -->
                                  </li>
                              </ul>
                            </div>
                          </div>
                          <div class="tab-pane fade" id="nav-message" role="tabpanel" aria-labelledby="nav-dropdown2-tab">
                            <div class="text">
                                Please <a href="#">login</a> / <a href="#">register</a> to send message.
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="related-product_wrapper">
                        <div class="title_wrapper">
                            <div class="single-title">
                                RELATED PRODUCTS
                            </div>
                        </div>
                        <div class="owl-nav">
                            <a class="owl-prev btn btn-owlnav mr-2">
                                <i class="pe-7s-angle-left"></i>
                            </a>
                            <a class="owl-next btn btn-owlnav">
                                <i class="pe-7s-angle-right"></i>
                            </a>
                        </div>
                        <div class="owl_wrapper list-product">
                            <div class="owl-carousel owl-theme owl-related">
                                <div class="item">
                                    <div class="product-box">
                                        <div class="img-product_wrapper">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-name mt-2">
                                               <a href="#"> MINI DESK</a>
                                            </div>
                                            <div class="bottom">
                                                <div class="product-price pull-left">
                                                    <span class="main-price">$72.00</span>
                                                    <span class="disc-price">$89.00</span>
                                                </div>
                                                <div class="product-rating pull-right">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="product-visiblebutton text-center">
                                            <div class="button-group">
                                                <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                <a href="#" class="btn btn-search"><i class="pe-7s-search"></i></a>
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                            </div>
                                        </div>
                                    </div><!--END PRODUCT BOX-->
                                </div><!--END ITEM-->
                                <div class="item">
                                    <div class="product-box">
                                        <div class="img-product_wrapper">
                                            <img src="images/relatedproduct.jpg" class="img-product img-fluid">
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-name mt-2">
                                               <a href="#"> MINI DESK</a>
                                            </div>
                                            <div class="bottom">
                                                <div class="product-price pull-left">
                                                    <span class="main-price">$72.00</span>
                                                    <span class="disc-price">$89.00</span>
                                                </div>
                                                <div class="product-rating pull-right">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="product-visiblebutton text-center">
                                            <div class="button-group">
                                                <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                <a href="#" class="btn btn-search"><i class="pe-7s-search"></i></a>
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                            </div>
                                        </div>
                                    </div><!--END PRODUCT BOX-->
                                </div><!--END ITEM-->

                                <div class="item">
                                    <div class="product-box">
                                        <div class="img-product_wrapper">
                                            <img src="images/tes.jpg" class="img-product img-fluid">
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-name mt-2">
                                               <a href="#"> MINI DESK</a>
                                            </div>
                                            <div class="bottom">
                                                <div class="product-price pull-left">
                                                    <span class="main-price">$72.00</span>
                                                    <span class="disc-price">$89.00</span>
                                                </div>
                                                <div class="product-rating pull-right">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="product-visiblebutton text-center">
                                            <div class="button-group">
                                                <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                <a href="#" class="btn btn-search"><i class="pe-7s-search"></i></a>
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                            </div>
                                        </div>
                                    </div><!--END PRODUCT BOX-->
                                </div><!--END ITEM-->
                            </div>
                        </div>
                    </div>
                </div><!--END MAIN CONTENT-->
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
         <script>
          $( function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [ 75, 300 ],
              slide: function( event, ui ) {
                $( "#amount" ).text( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            $( "#amount" ).text( "$" + $( "#slider-range" ).slider( "values", 0 ) +
              " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          } );

            $(document).ready(function(){
                var owlrelated = $('.owl-related');
                owlrelated.owlCarousel({
                    loop:true,
                    margin:30,
                    nav:false,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:2
                        },
                        1000:{
                            items:4
                        }
                    }
                });
                
                $('.owl-next').on('click', function(){
                    owlrelated.trigger('next.owl.carousel');
                });
                $('.owl-prev').on('click', function(){
                    owlrelated.trigger('prev.owl.carousel');
                });

                var owlmain = $('.owl-mainproduct');
                owlmain.owlCarousel({
                    loop:true,
                    margin:15,
                    nav:false,
                    dots:false,
                    responsive:{
                        0:{
                            items:3
                        },
                        600:{
                            items:3
                        },
                        1000:{
                            items:3
                        }
                    }
                });
                $('.owl-mainnext').on('click', function(){
                    owlmain.trigger('next.owl.carousel');
                });
                $('.owl-mainprev').on('click', function(){
                    owlmain.trigger('prev.owl.carousel');
                });
            });
          </script>
    </body>
</html>