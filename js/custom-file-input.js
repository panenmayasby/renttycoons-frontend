/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

;( function( $, window, document, undefined )
{
	$( '.inputfile' ).each( function()
	{
		var $input	 = $( this ),
			$span	 = $input.parent().find( 'span' ),
			$label	 = $input.closest('.form-group').find( '.filename' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName ){
				$span.html('UPLOADED');
				$label.html( fileName + ' success being uploaded &nbsp;<a class="orange removefile" id="removefile" onclick="deletefile()"><i class="pe-7s-close pe-va"></i> remove</a>');				
			}
			else
				$label.html( labelVal );
		});


		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );