<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-faq section-information">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">FAQ</li>
                    </ol>
                </div>
                <div class="main-content main-information">
                    <div class="main-title">Frequently Asked Question</div>
                    <div class="row information">
                        <div class="col-md-3 col-sm-4 nav-left_wrapper">
                            <nav class="nav flex-column nav-left">
                              <a class="nav-link active" href="#">About Rent Tycoons</a>
                              <a class="nav-link" href="#">New User Registration And Account Setup</a>
                              <a class="nav-link" href="#">Posting Items For Rent</a>
                              <a class="nav-link" href="#">Searching Items For Rent </a>
                              <a class="nav-link" href="#">Renting Items From Your Community</a>
                            </nav>
                        </div>
                        <div class="col-md-9 col-sm-8 info-right_wrapper">
                            <div class="info-box_wrapper">
                                <div class="card info-box active">
                                      <div class="card-header">
                                        What is Rent Tycoons?
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        How does Rent Tycoons work?
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        Why should you rent? 
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        What are the general terms of use?
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        How can I contact Rent Tycoons? 
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        What is Rent Tycoons’ Payment Processing System?
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        Why should you rent? 
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script>
             $(document).on('click', '.card-header', function(){ 
               $(this).parent().find(".card-body").slideToggle();
               $(this).parent().toggleClass('active');
           });
        </script>
    </body>
</html>