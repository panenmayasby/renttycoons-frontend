<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-requestitem">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                                Manage My Requested Items
                            </div>
                            <div class="top-button mb-3">
                                <div class="left pull-left">
                                    <a href="" class="btn btn-bggreen btn-square"><i class="pe-7s-search"></i> Display All Requested Item</a>
                                </div>
                                <div class="right pull-right">
                                    <a href="" class="btn btn-main btn-add"><i class="pe-7f-plus"></i> Add Item</a>
                                </div>
                                <div class='clearfix'></div>
                            </div>
                            <div class="table_wrapper">
                                <table class="table table-requestitem table-dashboard">
                                  <thead>
                                    <tr>
                                      <th>Name of Item</th>
                                      <th>Description</th>
                                      <th>Respond By</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                        <td>Blender Kitchen Aid</td>
                                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porta ut massa nec interdum. Pellentesque ut odio mi. Praesent id velit diam. </td>
                                        <td>05/09/2017</td>
                                        <td>
                                            <div class="action_wrapper">
                                                <div class="row-action mb-1">
                                                    <a href="#" class="action">
                                                        <div class="icon_wrapper">
                                                            <i class="pe-7s-close"></i>
                                                        </div>
                                                        <div class="text_wrapper">
                                                            Edit
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="row-action mb-1">
                                                    <a href="#" class="action">
                                                        <div class="icon_wrapper icon_delete">
                                                            <i class="pe-7s-trash"></i>
                                                        </div>
                                                        <div class="text_wrapper">
                                                            Delete
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Blender Kitchen Aid</td>
                                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porta ut massa nec interdum. Pellentesque ut odio mi. Praesent id velit diam. </td>
                                        <td>05/09/2017</td>
                                        <td>
                                            <div class="action_wrapper">
                                                <div class="row-action">
                                                    <a href="#" class="action">
                                                        <div class="icon_wrapper">
                                                            <i class="pe-7s-close"></i>
                                                        </div>
                                                        <div class="text_wrapper">
                                                            Edit
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="row-action">
                                                    <a href="#" class="action">
                                                        <div class="icon_wrapper icon_delete">
                                                            <i class="pe-7s-trash"></i>
                                                        </div>
                                                        <div class="text_wrapper">
                                                            Delete
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>