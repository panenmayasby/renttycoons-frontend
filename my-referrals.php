<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="dashboard dashboard-myreferrals">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                                My Referrals
                            </div>
                            <div class="top-button mb-3">
                                <div class="left pull-left">
                                    <a href="" class="btn btn-bggreen btn-square">Delete Selected</a>
                                </div>
                                <div class='clearfix'></div>
                            </div>
                            <div class="table_wrapper">
                                <table class="table table-myreferrals table-dashboard">
                                  <thead>
                                    <tr>
                                      <th>
                                        <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </th>
                                      <th>Select Date</th>
                                      <th>Invited</th>
                                      <th>Clicked</th>
                                      <th>Joined</th>
                                      <th>Rented</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </td>
                                       <td>
                                            2012/05/08 13:13:51
                                       </td>
                                       <td>lala321@hotmail.com</td>
                                       <td>No</td>
                                       <td>No</td>
                                       <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </td>
                                       <td>
                                            2012/05/08 13:13:51
                                       </td>
                                       <td>lala321@hotmail.com</td>
                                       <td>No</td>
                                       <td>No</td>
                                       <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </td>
                                       <td>
                                            2012/05/08 13:13:51
                                       </td>
                                       <td>lala321@hotmail.com</td>
                                       <td>No</td>
                                       <td>No</td>
                                       <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </td>
                                       <td>
                                            2012/05/08 13:13:51
                                       </td>
                                       <td>lala321@hotmail.com</td>
                                       <td>No</td>
                                       <td>No</td>
                                       <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </td>
                                       <td>
                                            2012/05/08 13:13:51
                                       </td>
                                       <td>lala321@hotmail.com</td>
                                       <td>No</td>
                                       <td>No</td>
                                       <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                            </label>
                                          </div> 
                                       </td>
                                       <td>
                                            2012/05/08 13:13:51
                                       </td>
                                       <td>lala321@hotmail.com</td>
                                       <td>No</td>
                                       <td>No</td>
                                       <td>No</td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>