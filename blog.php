<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-blog">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Blogs</li>
                    </ol>
                </div>
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="list-articles">
                                <div class="blog-box">
                                    <div class="img_wrapper">
                                        <img src="images/blog.jpg" class="img-fluid">
                                    </div>
                                    <div class="title"><a href="#">From Now we are certified web</a></div>
                                    <div class="date-author">
                                        <span class="date">2016/06/16</span>  |  &nbsp;<span>Josh Clark</span>
                                    </div> 
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
                                    </div>
                                    <div class="read-more">
                                        <a href="#" class="btn btn-readmore btn-grey">Read More</a>
                                    </div>
                                </div>
                                <div class="blog-box">
                                    <div class="img_wrapper">
                                        <img src="images/blog.jpg" class="img-fluid">
                                    </div>
                                    <div class="title"><a href="#">From Now we are certified web</a></div>
                                    <div class="date-author">
                                        <span class="date">2016/06/16</span>  |  &nbsp;<span>Josh Clark</span>
                                    </div> 
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
                                    </div>
                                    <div class="read-more">
                                        <a href="#" class="btn btn-readmore btn-grey">Read More</a>
                                    </div>
                                </div>
                                <div class="blog-box">
                                    <div class="img_wrapper">
                                        <img src="images/blog.jpg" class="img-fluid">
                                    </div>
                                    <div class="title"><a href="#">From Now we are certified web</a></div>
                                    <div class="date-author">
                                        <span class="date">2016/06/16</span>  |  &nbsp;<span>Josh Clark</span>
                                    </div> 
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
                                    </div>
                                    <div class="read-more">
                                        <a href="#" class="btn btn-readmore btn-grey">Read More</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="nav-oldnew">
                                <div class="pull-left older">
                                    <a href="#">
                                        <span class="icon"><i class="pe-7s-angle-left pe-fw"></i></span>&nbsp;OLDER POST
                                    </a>
                                </div>
                                <div class="pull-right newer">
                                    <a href="#">
                                       NEWER POST&nbsp; <span class="icon"><i class="pe-7s-angle-right pe-fw"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="recent-post">
                                <div class="title">Recent Posts</div>
                                <div class="first recent-box">
                                    <div class="img_wrapper"> <img src="images/recent1.jpg" class="img-fluid"></div>
                                    <div class="date">May 11, 2016</div>
                                    <div class="title-post"><a href="#">Beautiful Day with Friends in London Streets</a></div>
                                </div>
                                <div class="recent-box">
                                    <div class="media">
                                      <img src="images/recent2.jpg" class="d-flex align-self-center mr-2 img-fluid">
                                      <div class="media-body">
                                         <div class="date mt-0">May 11, 2016</div>
                                        <div class="title-post mb-0"><a href="#">Beautiful Day with Friends in London Streets</a></div>
                                      </div>
                                    </div>
                                </div>
                                <div class="recent-box">
                                    <div class="media">
                                      <img src="images/recent2.jpg" class="d-flex align-self-center mr-2 img-fluid">
                                      <div class="media-body">
                                         <div class="date mt-0">May 11, 2016</div>
                                        <div class="title-post mb-0"><a href="#">Beautiful Day with Friends in London Streets</a></div>
                                      </div>
                                    </div>
                                </div>
                                <div class="recent-box">
                                    <div class="media">
                                      <img src="images/recent2.jpg" class="d-flex align-self-center mr-2 img-fluid">
                                      <div class="media-body">
                                         <div class="date mt-0">May 11, 2016</div>
                                        <div class="title-post mb-0"><a href="#">Beautiful Day with Friends in London Streets</a></div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="newsletter">
            <div class="container">
                <div class="text-center text-main">
                    Sign up to <span>NewsLetter</span> and Get a <span>$50 Coupon</span>
                </div>
                <div class="newsletter-form">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Enter your email address" aria-label="Search for...">
                      <span class="input-group-btn">
                        <button class="btn btn-search" type="button"><i class="pe-7s-paper-plane"></i></button>
                      </span>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>