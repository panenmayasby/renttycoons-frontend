<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-sitemap">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Site Map</li>
                    </ol>
                </div>
                <div class="main-content">
                    <div class="main-title">Site Map</div>
                    <div class="sitemap-category">
                        <div class="title">Information Pages</div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        About Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Contact Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        FAQ
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Terms of Use
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        About Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Contact Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        FAQ
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Terms of Use
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sitemap-category">
                        <div class="title">Categories</div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        About Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Contact Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        FAQ
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Terms of Use
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        About Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Contact Us
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        FAQ
                                    </a>
                                </div>
                                <div class="col-md-3 col-6 site-box_wrapper">
                                    <a href="#" class="site-box btn-block">
                                        Terms of Use
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>