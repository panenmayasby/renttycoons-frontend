<header class="after-login">
		<div class="social-top_wrapper navbar">
			<div class="container">
				<div class="left pull-left">
					<ul class="nav">
					  <li class="nav-item">
					    <a href="#" class="btn btn-social-round"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					  </li>
					  <li class="nav-item">
					    <a href="#" class="btn btn-social-round"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					  </li>
					  <li class="nav-item">
					   	<a href="#" class="btn btn-social-round"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					  </li>
					  <li class="nav-item">
					    <a href="#" class="btn btn-social-round"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					  </li>
					</ul>
				</div>
				<div class="right pull-right">
					<ul class="nav nav-user">
					  <li class="nav-item">
					  	<div class="dropdown">
						  <a class="btn dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <i class="pe-7s-user pe-va"></i> Hi, <span class="green">Jessica</span>
						  </a>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						    <a class="dropdown-item" href="#">My Profile</a>
						    <a class="dropdown-item" href="#">Log Out</a>
						  </div>
						</div>
					  </li>
					  <li class="nav-item">
					    <a href="#" class="btn"><i class="pe-7s-cart pe-va"></i> <span class="badge badge-secondary badge-green">0</span></a>
					  </li>
					</ul>
				</div>
			</div>

		</div>
		<div class="nav-top navbar">
			<div class="container">
				<div class="logo_wrapper pull-left navbar-brand">
					<a href=""><img src="images/logo.png"></a>
				</div>
				<div class="navbar-right main-menu_wrapper">
					<ul class="nav">
					  <li class="nav-item"> 
					    <a class="nav-link active" href="#">My Dashboard</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">Messages</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">Manage Item</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">Post An Item</a>
					  </li>
					</ul>
				</div>
			</div>
		</div>
		<div class="nav-search">
			<div class="container">
				<div class="row">
					<div class="col col-md-9">
						<form>
							<div class="input-group">
						      <input type="text" class="form-control" placeholder="What are you looking for..." aria-label="Search for...">
						      <span class="input-group-btn">
						        <button class="btn btn-search" type="button"><i class="pe-7s-search"></i></button>
						      </span>
								<div class="dropdown show">
								  <a class="btn btn-categories dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    All Categories
								  </a>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
								    <a class="dropdown-item" href="#">Requested Item</a>
								  </div>
								</div>
						    </div>
						</form>
					</div>
					<div class="col col-md-3 contact-header">
						<div class="row">
							<div class="contact-left icon-hover_wrapper col-auto">
								<a href="#"><i class="pe-7s-headphones stroke"></i> <i class="pe-7f-headphones filled"></i></a>
							</div>
							<div class="contact-right">
								<div class="text">Support 24/7</div>
								<div class="icon-hover_wrapper">
									<a href="#"><i class="pe-7s-mail stroke"></i> <i class="pe-7f-mail filled"></i></a>-
									<a href="#"><i class="pe-7s-chat stroke"></i> <i class="pe-7f-chat filled"></i></a>-
									<a href="#"><i class="pe-7s-phone stroke"></i> <i class="pe-7f-phone filled" style="margin-left:2px;"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					</div>
			</div>
		</div>
</header>


