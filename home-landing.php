<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-home-landing">
            <div class="container">
                <div class="banner-top">
                    <div class="row">
                        <div class="col-md-6 bg-grey col-left">
                            <div class="row">
                                <div class="content align-self-center col-md-12">
                                    <div class="slogan">
                                        <span class="green">Be green</span> by renting from other today
                                    </div>
                                    <div class="search-home mt-3">
                                        <div class="input-group">
                                          <input type="text" class="form-control" placeholder="Search by item or postal code" aria-label="Product name">
                                          <span class="input-group-btn">
                                            <a class="btn btn-bggreen">explore</a>
                                          </span>
                                        </div>
                                        <div class="text mt-1">postal code will unlock available items within your vicinity</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 bg-transparent">
                        </div>
                    </div>
                </div>
                <div class="advantage padding-home">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="advantage-box pull-right">
                                <div class="green">
                                    Rent Tycoons 
                                </div>
                                <p class="mt-2">
                                    is an online marketplace that facilitates peer-to peer-renting in Singapore
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="advantage-list_wrapper">
                                <div class="title">Our portal enables you to do the following:</div>
                                <div class="list">
                                    <ul class="custom-list">
                                        <li>Make money by putting your items/services up for rent.</li>
                                        <li>Save money by searching and renting items/services that you
                                        need.</li>
                                        <li>Submit a request for desired items/services that are not yet
                                        available on the portal.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--END ADVANTAGE-->
                <div class="testimony">
                    <div class="title pull-right">
                        <div class="arrow-icon"><img src="images/icon/arrow.png"></div>
                        What people think ?
                    </div>
                    <div class="clearfix"></div>
                    <div class="content mt-4">
                        <div class="row no-gutters">
                            <div class="col-md-4 align-self-end">
                                <div class="testimony-box">
                                    <div class="bubble bubble1">
                                        <div class="text">
                                        "Asked for a jigsaw and had one within 30 min at 800m distance! Not bad for a first try."</div>
                                    </div>
                                    <div class="testimony-bottom">
                                        <div class="row no-gutters">
                                            <div class="col img-person">
                                                <img src="images/icon/newlyweeds.png" class="img-fluid">
                                            </div>
                                            <div class="col align-self-end name-position">
                                                <div class="name">
                                                    Mr <span>&amp;</span> mrs Smith
                                                </div>
                                                <div class="position">
                                                    newlyweeds
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END COL-->
                            <div class="col-md-4 align-self-end">
                                <div class="testimony-box">
                                    <div class="bubble bubble2">
                                        <div class="text">
                                        "Asked for a jigsaw and had one within 30 min at 800m distance! Not bad for a first try."</div>
                                    </div>
                                    <div class="testimony-bottom">
                                        <div class="row no-gutters">
                                            <div class="col img-person">
                                                <img src="images/icon/workers.png" class="img-fluid">
                                            </div>
                                            <div class="col align-self-end name-position">
                                                <div class="name">
                                                   John Doe
                                                </div>
                                                <div class="position">
                                                    Public Worker
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END COL-->
                            <div class="col-md-4 align-self-end">
                                <div class="testimony-box">
                                    <div class="bubble bubble3">
                                        <div class="text">
                                        "Asked for a jigsaw and had one within 30 min at 800m distance! Not bad for a first try."</div>
                                    </div>
                                    <div class="testimony-bottom">
                                        <div class="row no-gutters">
                                            <div class="col img-person">
                                                <img src="images/icon/Housewife.png" class="img-fluid">
                                            </div>
                                            <div class="col align-self-end name-position">
                                                <div class="name">
                                                    Mrs Wellington
                                                </div>
                                                <div class="position">
                                                    Housewife
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END COL-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div><!--END TESTIMONY-->
                <div class="why">
                    <div class="title">Why use rent tycoons?</div>
                    <div class="content text-center">
                        <div class="why-button_wrapper why-button-left why-top-1">
                            <div class="why-button">
                                <div class="input-group">
                                  <span type="text" class="input-group-addon why-text">Save Money</span>
                                  <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/piggy-bank.png"></span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->
                        <div class="why-button_wrapper why-button-right why-top-1">
                            <div class="why-button">
                                <div class="input-group">
                                    <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/lock.png"></span>
                                  <span type="text" class="input-group-addon why-text">Secure</span>
                                  
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->
                        <div class="why-button_wrapper why-button-left why-top-2">
                            <div class="why-button">
                                <div class="input-group">
                                  <span type="text" class="input-group-addon why-text">Make Money</span>
                                  <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/money.png"></span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->
                        <div class="why-button_wrapper why-button-right why-top-2">
                            <div class="why-button">
                                <div class="input-group">
                                  <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/wrench.png"></span>
                                  <span type="text" class="input-group-addon why-text">Convenient</span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->

                        <div class="why-button_wrapper why-button-left why-top-3">
                            <div class="why-button">
                                <div class="input-group">
                                  <span type="text" class="input-group-addon why-text">Social</span>
                                  <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/social.png"></span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->

                        <div class="why-button_wrapper why-button-right why-top-3">
                            <div class="why-button">
                                <div class="input-group">
                                    <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/cart.png"></span>
                                    <span type="text" class="input-group-addon why-text">Marketplace</span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->

                        <div class="why-button_wrapper why-button-left why-top-4">
                            <div class="why-button">
                                <div class="input-group">
                                  <span type="text" class="input-group-addon why-text">Be Green</span>
                                  <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/tree.png"></span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->

                         <div class="why-button_wrapper why-button-right why-top-4">
                            <div class="why-button">
                                <div class="input-group">
                                    <span class="input-group-addon why-icon" id="basic-addon2"><img src="images/icon/free.png"></span>
                                    <span type="text" class="input-group-addon why-text">Free</span>
                                </div>
                                <div class="hover-text">
                                    <div class="top">
                                        You only need it for a shortterm,
                                    </div>
                                    <div class="bottom">
                                        Why Buy ?
                                    </div>
                                </div>
                            </div>
                        </div><!--END BUTTON WRAPPER-->
                        <img src="images/icon/why.png" class="">
                    </div>
                    
                </div>
                <div class="tutorial mb-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="img_wrapper pull-right">
                                <img src="images/icon/arrow.png" width="80px">
                            </div>
                            <div class="clearfix"></div>
                            <div class="text-slogan">
                                <span class="top">Renting is</span>
                                <span class="bottom">Simple</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top">
                                <div class="media">
                                  <img class="d-flex align-self-center mr-3" src="images/icon/landing-man.png" alt="Generic placeholder image">
                                  <div class="media-body d-flex align-self-center text-center ml-3">
                                    Renter
                                  </div>
                                </div>
                            </div>
                            <div class="bottom">
                                <ol class="tutorial-list">
                                    <li>
                                        <span>Search the item</span>
                                    </li>
                                    <li>
                                        <span>Contact owner to make an appointment.</span>
                                    </li>
                                    <li>
                                        <span>Reserve and Made the payment.</span>
                                    </li>
                                    <li>
                                        <span>Collect or wait till the item is delivered. Done!</span>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-md-1"> 
                            <img src="images/icon/landingicon.png" class="mt-4">
                        </div>
                        <div class="col-md-3">
                            <div class="top">
                                <div class="media">
                                  <div class="media-body d-flex align-self-center text-center ml-4">
                                    Owner
                                  </div>
                                  <img class="d-flex align-self-center mr-3" src="images/icon/landing-woman.png" alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="bottom">
                                <ol class="tutorial-list">
                                    <li>
                                        <span>Take a photo of the item</span>
                                    </li>
                                    <li>
                                        <span>Set your rental Fee & Deposit Describe your item and Upload</span>
                                    </li>
                                    <li>
                                        <span>Done! You are in business now.</span>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>