<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-requesteditems">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                                Requested Items
                            </div>
                            <div class="top-button mb-4">
                                <div class="left pull-left">
                                    <form class="form-inline">
                                      <div class="form-group form-keyword">
                                        <label for="staticEmail2" class="col-sm-2 col-form-label">Keyword:</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                              <input type="text" class="form-control" id="staticEmail2" placeholder="Enter Keyword Here">
                                              <span class="input-group-btn">
                                                <a class="btn btn-square btn-bggreen"><i class="pe-7s-search"></i></a>
                                              </span>
                                            </div>
                                        </div>

                                      </div>
                                    </form>
                                </div>
                                <div class="right pull-right">
                                    <a href="" class="btn btn-bggreen btn-square">Request An Item</a>
                                </div>
                                <div class='clearfix'></div>
                            </div>
                            <div class="table_wrapper">
                                <table class="table table-requesteditems table-dashboard">
                                  <thead>
                                    <tr>
                                      <th>Name of Item</th>
                                      <th>Description</th>
                                      <th>Respond By</th>
                                      <th>Send Message</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                        <td>Blender Kitchen Aid</td>
                                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porta ut massa nec interdum. Pellentesque ut odio mi. Praesent id velit diam. </td>
                                        <td>05/09/2017</td>
                                        <td>
                                            <div class="action_wrapper">
                                                <div class="row-action">
                                                    <a href="#" class="action">
                                                        <div class="icon_wrapper">
                                                            <i class="pe-7s-mail"></i>
                                                        </div>
                                                        <div class="text_wrapper">
                                                            Send
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                            <div class="pagination_wrapper mt-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>