<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-editprofile">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                                Edit My Profile
                            </div>
                            <div class="sub-title">
                                Display Information
                            </div>
                            <div class="form_wrapper">
                              <form> 
                                <div class="row row_form">
                                    <div class="col-md-6">
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Display Name:</label>
                                        <div class="col-sm-8">
                                          <input type="text" readonly class="form-control" id="staticEmail" value="jessijean92">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Nearest MRT Station:</label>
                                        <div class="col-sm-8">
                                           <select id="inputState" class="form-control">
                                                <option disabled selected>Select Station</option>
                                                <option>Bugis</option>
                                                <option>Rochor</option>
                                                <option>Little India</option>
                                                <option>Harbour Front</option>
                                                <option>Chinatown</option>
                                                <option>Orchard</option>
                                                <option>Somerset</option>
                                                <option>Marina Bay</option>
                                                <option>Expo</option>
                                            </select>
                                        </div>
                                      </div>   
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                          <label for="staticEmail" class="col-sm-4 col-form-label">Postal Code:</label>
                                          <div class="col-sm-8">
                                            <input type="text" class="form-control" id="staticEmail" value="12345">
                                          </div>
                                        </div>
                                    </div>                           
                                </div>
                              </form>
                            </div>
                            <div class="sub-title">
                                Personal Information
                            </div>
                            <div class="form_wrapper">
                              <form> 
                                <div class="row row_form">
                                    <div class="col-md-6">
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Email:</label>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" id="staticEmail" value="jessijean92">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Name:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" value="Jessica">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Last Name:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="Fill Your Last Name">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">ID Type:</label>
                                        <div class="col-sm-8">
                                          <select id="inputState" class="form-control">
                                                <option disabled selected>Select ID Type</option>
                                                <option>KTP</option>
                                                <option>SIM</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">ID Number:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="eg: 12345">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Phone Number:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="+02">
                                        </div>
                                      </div> 
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Address:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="Fill Your Address">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Postal Code:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" value="21312">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">City:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="Fill Your City">
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Country:</label>
                                        <div class="col-sm-8">
                                           <select id="inputState" class="form-control">
                                                <option disabled selected>Select Country</option>
                                                <option>Bugis</option>
                                                <option>Rochor</option>
                                                <option>Little India</option>
                                                <option>Harbour Front</option>
                                                <option>Chinatown</option>
                                                <option>Orchard</option>
                                                <option>Somerset</option>
                                                <option>Marina Bay</option>
                                                <option>Expo</option>
                                            </select>
                                        </div>
                                      </div>  
                                      <div class="form-group row mb-1">
                                        <label for="staticEmail" class="col-sm-4">Bank Account for Admin to Pay Owner:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="staticEmail" placeholder="Exp: DBS Current 011-903573-0">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <a href="#" class="btn btn-block btn-pass"><i class="pe-7s-lock"></i>Change Password</a>
                                      </div>
                                    </div>                           
                                </div>
                                <div class="button_wrapper mt-4">
                                  <a href="#" class="btn btn-square btn-bggreen pull-right">Save</a>
                                  <div class="clearfix"></div>
                                </div>
                              </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>