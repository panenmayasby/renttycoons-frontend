<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-manageitem-owner dashboard-manageitem">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="nav-manageitem mb-3">
                                <nav>
                                  <ul class="nav">
                                    <li class="nav-item">
                                      <a class="nav-link" href="#">Inventory</a>
                                    </li>
                                    <li class="nav-item active">
                                      <a class="nav-link active" href="#">Rented to Others (I am an Owner)</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="#">Rented from Others (I am a Renter)</a>
                                    </li>
                                  </ul>
                                </nav>
                            </div>
                            <div class="pagination_wrapper mt-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                                <div class="status-filter col-md-4 ml-md-auto pr-0">
                                  Status : 
                                  <select id="inputState" class="sort-status">
                                      <option>All</option>
                                      <option>Accepted</option>
                                      <option>Rejected</option>
                                  </select>
                                </div>
                            </div>
                            <div class="manage-item">
                              <div class="item_wrapper">
                                <div class="row">
                                    <div class="col-md-8 left pr-2">
                                      <div class="media">
                                        <img class="d-flex mr-3 align-self-center" src="images/item.jpg" alt="Item Name">
                                        <div class="media-body">
                                          <div class="mt-0 product-name mb-2">Center-aligned media</div>
                                          <p class="mb-2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                          <p class="mb-0">
                                            Status: <a href="#">Pending Payment</a>
                                          </p>
                                          <p class="mb-3">
                                            e-Contract #2630 (Owner Name: <a href="#" class="user-name">rachelqiukexin-76</a>)
                                          </p>
                                          <p class="media-bottom">
                                            <span>S$ 5 / day</span>|<span>S$ 27 / week</span>|<span>S$ 100 / month</span>
                                          </p>
                                        </div>
                                      </div><!--END MEDIA-->
                                    </div><!--END COL-->
                                    <div class="col-md-4 right status_wrapper pl-2">
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Status
                                        </div>
                                        <div class="col-7">
                                          : <span class="green">Reserved</span>
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Payment Method 
                                        </div>
                                        <div class="col-7">
                                          : Bank
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Payment Status
                                        </div>
                                        <div class="col-7">
                                          : <span class="orange">Not Claimed</span>
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Rental Period
                                        </div>
                                        <div class="col-7">
                                          : 2017-09-05 - 2017-09-05
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Rental Fee
                                        </div>
                                        <div class="col-7">
                                          : S$ 25
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Deposit Fee
                                        </div>
                                        <div class="col-7">
                                          : S$ 200
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Total
                                        </div>
                                        <div class="col-7">
                                          : S$ 225 (Before Discount)
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Coupon/Discount 
                                        </div>
                                        <div class="col-7">
                                          : NIL
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Total 
                                        </div>
                                        <div class="col-7">
                                          : S$ 225 (After Discount)
                                        </div>
                                      </div><!--END ROW-->
                                    </div><!--END COL-->
                                </div><!--END ROW-->
                                <div class="item-bottom mt-3">
                                    <nav class="nav">
                                      <a class="nav-link" href="#"><i class="pe-7s-close pe-va" style="font-size:22px;margin-right:0px;"></i> Delist</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-mail pe-va"></i> Message Renter</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-news-paper pe-va"></i> View e-Contract</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-look pe-va"></i> View Item Page</a>
                                    </nav>
                                </div>
                              </div><!--END ITEM-->
                              <div class="item_wrapper">
                                <div class="row">
                                    <div class="col-md-8 left pr-2">
                                      <div class="media">
                                        <img class="d-flex mr-3 align-self-center" src="images/item.jpg" alt="Item Name">
                                        <div class="media-body">
                                          <div class="mt-0 product-name mb-2">Center-aligned media</div>
                                          <p class="mb-2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                          <p class="mb-0">
                                            Status: <a href="#">Pending Payment</a>
                                          </p>
                                          <p class="mb-3">
                                            e-Contract #2630 (Owner Name: <a href="#" class="user-name">rachelqiukexin-76</a>)
                                          </p>
                                          <p class="media-bottom">
                                            <span>S$ 5 / day</span>|<span>S$ 27 / week</span>|<span>S$ 100 / month</span>
                                          </p>
                                        </div>
                                      </div><!--END MEDIA-->
                                    </div><!--END COL-->
                                    <div class="col-md-4 right status_wrapper pl-2">
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Status
                                        </div>
                                        <div class="col-7">
                                          : <span class="green">Complete</span>
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Payment Method 
                                        </div>
                                        <div class="col-7">
                                          : Bank
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Payment Status
                                        </div>
                                        <div class="col-7">
                                          : <span class="green">Paid</span>
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Rental Period
                                        </div>
                                        <div class="col-7">
                                          : 2017-09-05 - 2017-09-05
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Rental Fee
                                        </div>
                                        <div class="col-7">
                                          : S$ 25
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Deposit Fee
                                        </div>
                                        <div class="col-7">
                                          : S$ 200
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Total
                                        </div>
                                        <div class="col-7">
                                          : S$ 225 (Before Discount)
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Coupon/Discount 
                                        </div>
                                        <div class="col-7">
                                          : NIL
                                        </div>
                                      </div><!--END ROW-->
                                      <div class="row row_status no-gutters">
                                        <div class="col-5">
                                          Contract Total 
                                        </div>
                                        <div class="col-7">
                                          : S$ 225 (After Discount)
                                        </div>
                                      </div><!--END ROW-->
                                    </div><!--END COL-->
                                </div><!--END ROW-->
                                <div class="item-bottom mt-3">
                                    <nav class="nav">
                                      <a class="nav-link" href="#"><i class="pe-7s-close pe-va" style="font-size:22px;margin-right:0px;"></i> Delist</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-mail pe-va"></i> Message Renter</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-news-paper pe-va"></i> View e-Contract</a>
                                      <a class="nav-link" href="#"><i class="pe-7s-look pe-va"></i> View Item Page</a>
                                    </nav>
                                </div>
                              </div><!--END ITEM-->
                            </div><!--END MANAGE ITEM-->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>