<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-invitefriends">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                                Invite Friends
                            </div>
                            <div class="top-button mb-4 mt-1">
                                <span>Please use comma(,) if you want to send to multiple email addresses.(e.g. James@gmail.com,Mary@live.com)</span>
                                <div class='clearfix'></div>
                            </div>
                            <div class="form_wrapper">
                              <form>
                                <div class="form-group row">
                                  <label for="staticEmail" class="col-sm-2 col-form-label">Emails:</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control" id="staticEmail">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="inputPassword" class="col-sm-2 col-form-label">Subjects:</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputPassword" value="Join Rent Tycoons and be part of the leading peer-to-peer renting in Singapore!">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="inputPassword" class="col-sm-2 col-form-label">Message:</label>
                                  <div class="col-sm-10">
                                    <textarea class="form-control" rows="13">
Hello,

Would you like to earn some extra income or to save money? I just discovered this brand new website that allows you to earn money by renting out your items or to rent interesting items instead of buying them. 
Do check it out and become part of this fun community at www.RentTycoons.com. I am already a member and I recommend that you try it too.

Click on the green button below Accept The Invite 
<b>Regards,
Jessica </b>
                                  </textarea>
                                  </div>
                                </div>
                                <div class="bottom_wrapper mt-4">
                                  <div class="share pull-left">
                                    <span>Share This</span>
                                    <div class="social">
                                      <a href="#" class="btn"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                      <a href="#" class="btn"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                      <a href="#" class="btn"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                      <a href="#" class="btn"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                    </div>
                                  </div>
                                  <div class="button_wrapper pull-right">
                                    <a href="" class="btn btn-bggreen btn-square">Send</a>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>