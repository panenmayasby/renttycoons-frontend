<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-listproduct section-renterprofile">
            <div class="container">

                <div class="main-content">
                    <div class="row list-product">
                        <div class="sidebar_wrapper col-md-3">
                            <div class="sidebar">
                                <nav class="nav flex-column">
                                  <a class="nav-link active" href="#">COOKER</a>
                                  <a class="nav-link" href="#">Cleaning</a>
                                  <a class="nav-link" href="#">Kitchen Tools</a>
                                  <a class="nav-link" href="#">Bowls</a>
                                  <a class="nav-link" href="#">Refrigrator</a>
                                  <a class="nav-link" href="#">Blender</a>
                                </nav>
                            </div>
                        </div><!--END SIDEBAR-->
                        <div class="list_product_right col-md-9">
                            <div class="title-border title-user-information">
                                User Information
                            </div>
                            <div class="user-information">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-6 col-form-label">Display name</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-5">
                                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="jessijean92">
                                            </div>
                                        </div>
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-6 col-form-label">Member since</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-5">
                                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="Mar 2017">
                                            </div>
                                        </div><!--FORM GROUP-->
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-6 col-form-label">Postal Code</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-5">
                                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="55051">
                                            </div>
                                        </div><!--FORM GROUP-->
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-6 col-form-label">Nearest MRT</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-5">
                                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="Ang mo kio">
                                            </div>
                                        </div><!--FORM GROUP-->
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-12 col-form-label">No. of Feedback</label>
                                        </div>
                                        <div class="form-group row no-gutters feedback-icon_wrapper">
                                            <div class="col-sm-12">
                                                <img src="images/feedback-smile2.png"><span class="ml-2 green">12</span>
                                            </div>
                                        </div>
                                        <div class="form-group row no-gutters feedback-icon_wrapper">
                                            <div class="col-sm-12">
                                                <img src="images/feedback-flat.png"><span class="ml-2 grey">12</span>
                                            </div>
                                        </div>
                                        <div class="form-group row no-gutters feedback-icon_wrapper">
                                            <div class="col-sm-12">
                                                <img src="images/sad-red.png"><span class="ml-2 orange">12</span>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="col-md-5">
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-6 col-form-label">Tycoon Average Rating :</label>
                                            <div class="col-sm-6">
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <span class="green ml-1">(4.2)</span>
                                                </div>
                                            </div>
                                        </div><!--END FORM GROUP-->
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Responsiveness</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-6">
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <span class="ml-1">(4.2)</span>
                                                </div>
                                            </div>
                                        </div><!--END FORM GROUP-->
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Timeliness</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-6">
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <span class="ml-1">(4.2)</span>
                                                </div>
                                            </div>
                                        </div><!--END FORM GROUP-->

                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Courteous</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-6">
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <span class="ml-1">(4.2)</span>
                                                </div>
                                            </div>
                                        </div><!--END FORM GROUP-->
                                        <div class="form-group row no-gutters">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Realibility</label>
                                            <label for="staticEmail" class="col-sm-1 col-form-label text-center">:</label>
                                            <div class="col-sm-6">
                                                <div class="product-rating">
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star active"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <i class="pe-7s-star"></i>
                                                    <span class="ml-1">(4.2)</span>
                                                </div>
                                            </div>
                                        </div><!--END FORM GROUP-->
                                   </div>
                                </div>
                            </div>
                            <div class="title-border mb-4">
                                Items
                            </div>
                            <div class="list-product mt-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="mr-3 col-media">
                                                    <div class="img-product_wrapper d-flex align-self-center">
                                                        <img class="img-product img-fluid" src="images/tes.jpg" alt="Generic placeholder image">
                                                    </div>
                                              </div>
                                              <div class="media-body d-flex align-self-center">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <p class="green avail-status">
                                                        Available
                                                    </p>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00 / day</span>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="mr-3 col-media">
                                                    <div class="img-product_wrapper d-flex align-self-center">
                                                        <img class="img-product img-fluid" src="images/news-banner.jpg" alt="Generic placeholder image">
                                                    </div>
                                              </div>
                                              <div class="media-body d-flex align-self-center">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <p class="green avail-status">
                                                        Available
                                                    </p>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00 / day</span>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="d-flex align-self-center mr-3 col-media">
                                                    <img class="img-product img-fluid" src="images/list-product.png" alt="Generic placeholder image">
                                              </div>
                                              <div class="media-body d-flex align-self-center">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <p class="green avail-status">
                                                        Available
                                                    </p>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00 / day</span>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                </div>
                            </div>
                            <div class="pagination_wrapper mt-4">
                                <div class="col space"></div>
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-5 button-pagination">
                                    <nav aria-label="Page navigation example pull-right">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--END MAIN CONTENT-->
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
         <script>
          $( function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [ 75, 300 ],
              slide: function( event, ui ) {
                $( "#amount" ).text( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            $( "#amount" ).text( "$" + $( "#slider-range" ).slider( "values", 0 ) +
              " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          } );
          </script>
    </body>
</html>