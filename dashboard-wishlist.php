<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-wishlist">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                      <a class="nav-link" href="#">My Favourite Item(s)/ Owner(s)</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="title">
                              Wishlist
                            </div>
                            
                            <div class="list-product mt-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <div class="img-product_wrapper">
                                                <img src="images/news-banner.jpg" class="img-product img-fluid">
                                            </div>
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="promote">
                                                <i class="pe-7s-speaker"></i>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="promote">
                                                <i class="pe-7s-speaker"></i>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <div class="img-product_wrapper">
                                                <img src="images/tes.jpg" class="img-product img-fluid">
                                            </div>
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="promote">
                                                <i class="pe-7s-speaker"></i>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-4">
                                        <div class="product-box">
                                            <img src="images/list-product.jpg" class="img-product img-fluid">
                                            <div class="product-desc">
                                                <div class="product-name mt-2">
                                                   <a href="#"> MINI DESK</a>
                                                </div>
                                                <div class="bottom">
                                                    <div class="product-price pull-left">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="product-rating pull-right">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="product-visiblebutton text-center">
                                                <div class="button-group">
                                                    <a href="#" class="btn btn-rent"><i class="pe-7s-cart"></i>&nbsp;&nbsp;Rent Now</a>
                                                    
                                                    <a href="#" class="btn btn-search"><i class="pe-7s-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                </div>
                            </div>
                            <div class="pagination_wrapper mt-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination mr-auto ml-auto">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>