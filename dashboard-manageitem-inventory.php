<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header-login.php'); ?>
        <section class="dashboard dashboard-manageitem-inventory dashboard-manageitem">
            <div class="container">
                <div class="main-content">
                    <div class="row">
                        <div class="col-md-3 sidebar-dashboard_wrapper">
                            <div class="sidebar-dashboard">
                                <div class="title">
                                    <a href="#"><i class="pe-7f-menu"></i> BACK TO HOME</a>
                                </div>
                                <div class="content">
                                    <nav class="nav flex-column">
                                      <a class="nav-link active" href="#">My Dashboard</a>
                                      <a class="nav-link" href="#">Manage Item</a>
                                      <a class="nav-link" href="#">Request an Item</a>
                                      <a class="nav-link" href="#">Invite Friends</a>
                                      <a class="nav-link" href="#">View Referrals</a>
                                      <a class="nav-link" href="#">My Messages</a>
                                      <a class="nav-link" href="#">Update My Profile</a>
                                      <a class="nav-link" href="#">Evaluate Item/ Owner/ Renter</a>
                                      <a class="nav-link" href="#">Claim Rental Income</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 content-dashboard">
                            <div class="nav-manageitem mb-3">
                                <nav>
                                  <ul class="nav">
                                    <li class="nav-item active">
                                      <a class="nav-link" href="#">Inventory</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="#">Rented to Others (I am an Owner)</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="#">Rented from Others (I am a Renter)</a>
                                    </li>
                                  </ul>
                                </nav>
                            </div>
                            <div class="pagination_wrapper mt-4 mb-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                                <div class="status-filter col-md-4 ml-md-auto pr-0">
                                  Status : 
                                  <select id="inputState" class="sort-status">
                                      <option>All</option>
                                      <option>Accepted</option>
                                      <option>Rejected</option>
                                  </select>
                                </div>
                            </div>
                            <div class="action_wrapper mb-2">
                              <div class="action-select pull-left">
                                Action : 
                                  <select id="inputState" class="select-action">
                                      <option disabled selected>Select Action</option>
                                      <option>Accepted</option>
                                      <option>Rejected</option>
                                  </select>
                                  <a href="#" class="btn btn-action">OK</a>
                              </div>
                              <div class="add-items pull-right">
                                  <div class="button_wrapper">
                                      <a href="" class="btn btn-main btn-add"><i class="pe-7f-plus"></i> Add New Item</a>
                                  </div>
                                  <div class="button_wrapper">
                                     <a href="" class="btn btn-main btn-add pr-0" data-toggle="modal" data-target="#modal-multiple"><i class="pe-7f-plus"></i> Add Multiple Items</a>
                                  </div>
                              </div>
                              <div class="clearfix"></div>
                            </div>
                            <div class="manage-item">
                              <div class="item_wrapper">
                                <div class="row">
                                    <div class="col-md-1 checkbox_wrapper pr-0 align-self-center">
                                      <div class="form-check">
                                        <label class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input">
                                          <span class="custom-control-indicator"></span>
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-8 left pr-2">
                                      <div class="media">
                                        <img class="d-flex mr-3 align-self-center" src="images/item.jpg" alt="Item Name">
                                        <div class="media-body">
                                          <div class="mt-0 product-name mb-2">Center-aligned media</div>
                                          <p class="mb-3">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.Cras sit amet nibh libero, in gravida nulla.</p>
                                          <p class="mb-2">
                                            Status: <a href="#">Available</a>
                                          </p>
                                          <p class="media-bottom">
                                            <span>S$ 5 / day</span>|<span>S$ 27 / week</span>|<span>S$ 100 / month</span>
                                          </p>
                                        </div>
                                      </div><!--END MEDIA-->
                                    </div><!--END COL-->
                                    <div class="col-md-3 right status_wrapper pl-2 link-right align-self-center">
                                      <nav class="nav">
                                        <a class="nav-link" href="#"><i class="pe-7s-close pe-va" style="font-size:22px;margin-left:-5px;"></i> Delist</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-look pe-va"></i> View Item Page</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-pen pe-va"></i> Edit</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-trash pe-va"></i> Delete</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-share pe-va"></i> Share</a>
                                      </nav>
                                    </div><!--END COL-->
                                </div><!--END ROW-->
                              </div><!--END ITEM-->
                              <div class="item_wrapper">
                                <div class="row">
                                    <div class="col-md-1 checkbox_wrapper pr-0 align-self-center">
                                      <div class="form-check">
                                        <label class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input">
                                          <span class="custom-control-indicator"></span>
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-8 left pr-2">
                                      <div class="media">
                                        <img class="d-flex mr-3 align-self-center" src="images/item.jpg" alt="Item Name">
                                        <div class="media-body">
                                          <div class="mt-0 product-name mb-2">Center-aligned media</div>
                                          <p class="mb-3">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.Cras sit amet nibh libero, in gravida nulla.</p>
                                          <p class="mb-2">
                                            Status: <a href="#">Available</a>
                                          </p>
                                          <p class="media-bottom">
                                            <span>S$ 5 / day</span>|<span>S$ 27 / week</span>|<span>S$ 100 / month</span>
                                          </p>
                                        </div>
                                      </div><!--END MEDIA-->
                                    </div><!--END COL-->
                                    <div class="col-md-3 right status_wrapper pl-2 link-right align-self-center">
                                      <nav class="nav">
                                        <a class="nav-link" href="#"><i class="pe-7s-close pe-va" style="font-size:22px;margin-left:-5px;"></i> Delist</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-look pe-va"></i> View Item Page</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-pen pe-va"></i> Edit</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-trash pe-va"></i> Delete</a>
                                        <a class="nav-link" href="#"><i class="pe-7s-share pe-va"></i> Share</a>
                                      </nav>
                                    </div><!--END COL-->
                                </div><!--END ROW-->
                              </div><!--END ITEM-->
                            </div><!--END MANAGE ITEM-->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>

        <!-- Modal LOGIN -->
        <div class="modal fade modal-upgrade" id="modal-upgrade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Upgrade to Business Account</div>
                <div class="content">
                  <p>
                    Upgrade to Business Account will allow you to enjoy multiple 
                    benefists such as:
                  </p>
                  <ul>
                    <li>Dedicated account manager 7 x 24 ready to serve you
                    </li>
                    <li>Upload your items in bulk
                    </li>
                  </ul>
                  <div class="button_wrapper mt-4">
                    <a href="#" class="btn btn-bggreen btn-block btn-square">UPGRADE</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--END MODAL-->

        <!-- Modal Payment Method -->
        <div class="modal fade modal-upgrade modal-paymentmethod" id="modal-paymentmethod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-med" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Upgrade to Business Account</div>
                <div class="content">
                  <div class="total-payment mb-3">
                      Total payment: S$ 115.00
                  </div>
                  <div class="payment-option">
                      Please choose your payment option here:
                      <div class="row mt-3">
                          <div class="col-md-5">
                              <form> 
                                  <div class="form-group row row_form no-gutters">
                                      <div class="col-auto radio-custom_wrapper pt-0 pb-0">
                                          <label class="custom-control custom-radio">
                                            <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                          </label>
                                      </div>
                                      <div class="col-10 custom-control-description">
                                          <label for="staticEmail" class="col-form-label pt-1 mb-2">Bank Transfer:</label>
                                          <select id="inputState" class="form-control">
                                              <option disabled selected>Select Bank</option>
                                              <option>DBS</option>
                                              <option>OCBC</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group row row_form no-gutters mt-4">
                                      <div class="col-auto radio-custom_wrapper pt-0 pb-0">
                                          <label class="custom-control custom-radio">
                                            <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                          </label>
                                      </div>
                                      <div class="col-10 custom-control-description">
                                          <label for="staticEmail" class="col-form-label pt-1 mb-2">Paypal</label>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <div class="col-md-7 border-left align-self-center">
                              Account Name : Rent Tycoons Pte. Ltd.</br>
                              Account Number : 003-943683-1 (Acct.Type:Current)</br>
                              Bank Code : 7171</br>
                              Branch Code : 003
                          </div>
                      </div>
                  </div>
                  <div class="button_wrapper mt-4">
                    <a href="#" class="btn btn-bggreen btn-block btn-square">UPGRADE</a>
                  </div>
                </div><!--END CONTENT-->
              </div>
            </div>
          </div>
        </div>
        <!--END MODAL-->

        <!-- Modal SUCCESS -->
        <div class="modal fade modal-upgrade" id="modal-upgradesuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Upgrade Successful</div>
                <div class="content">
                  <p>
                    Your reservation is successful and will expire within 6 hours. If you accidently close the page, look for your email to confirm your payment!
                  </p>
                  <div class="button_wrapper mt-4">
                    <a href="#" class="btn btn-bggreen btn-block btn-square" data-toggle="modal" data-target="#modal-paymentconfirmation">PAYMENT CONFIRMATION</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--END MODAL-->

         <!-- Modal PAYMENT CONFIRMATION -->
        <div class="modal fade modal-upgrade modal-paymentconfirmation" id="modal-paymentconfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Payment Confirmation</div>
                <div class="content">
                  <div class="form_wrapper form_payment">
                    <form>
                      <div class="row_form">
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Date of Transfer:</label>
                          <div class="col-sm-8 date_wrapper">
                            <input type="text" class="form-control date-picker date-transfer" id="date-transfer">
                            <i class="pe-7s-browser"></i>
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label pt-0 pr-0">Internet Bank Transfer Reference Number:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="staticEmail" value="">
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Transferred to:</label>
                          <div class="col-sm-8">
                            <select id="inputState" class="form-control">
                                  <option disabled selected>Select Bank</option>
                                  <option>DBS</option>
                                  <option>OCBC</option>
                              </select>
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Transaction Amount (SGD):</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="staticEmail" value="">
                          </div>
                        </div><!--END FORM GROUP-->
                        <div class="form-group row">
                          <label for="staticEmail" class="col-sm-4 col-form-label">Remarks:</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="staticEmail" placeholder="">
                          </div>
                        </div><!--END FORM GROUP-->
                      </div>
                    </form>
                  </div>
                  <div class="button_wrapper mt-4">
                    <a href="#" class="btn btn-bggreen btn-block btn-square">PAYMENT CONFIRMATION</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--END MODAL-->

        <!-- Modal MULTIPLE -->
        <div class="modal fade modal-upgrade modal-multiple" id="modal-multiple" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="title">Upgrade Multiple Items</div>
                <div class="content">
                  <p>
                    This feature enables you to upload multiple items at once.
                    Please ensure you use the format in the provided csv template. 
                  </p>
                  <p class="upload-link">
                    <a href="#" class="mr-2"><i class="pe-7s-look"></i>&nbsp;Upload Instruction</a>
                    <a href="#"><i class="pe-7s-download"></i>&nbsp;Download CSV Template</a>
                  </p>
                  <div class="form_wrapper">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label align-self-center">Item Category:</label>
                        <div class="col-sm-8">
                          <select id="inputState" class="form-control">
                              <option disabled selected>Select Bank</option>
                              <option>DBS</option>
                              <option>OCBC</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label align-self-center">Upload File</label>
                        <div class="col-sm-8">
                          <div class="input-file_wrapper">
                            <div class="fileUpload uploadktp btn btn-uploadfile btn-block">
                                <span>Upload</span>
                                <input type="file" class="upload inputfile">
                            </div>
                          </div><!--INPUT FILE WRAPPER-->
                        </div>
                        <div class="filename col-sm-12">No File Chosen</div>
                      </div>
                  </div>
                  <div class="button_wrapper mt-4">
                    <a href="#" class="btn btn-bggreen btn-block btn-square">ADD</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--END MODAL-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script> 
        <script type="text/javascript" src="js/moment.js"></script>
        <script type="text/javascript" src="js/daterangepicker.js"></script>
        <script type="text/javascript" src="js/custom-file-input.js"></script>
        <script>
            $(document).ready(function() {
              $('.date-transfer').daterangepicker({
                  "singleDatePicker": true,
                  "startDate": "10/12/2017",
                  "endDate": "09/29/2017"
              }, function(start, end, label) {
                console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
              });

            });
            
            function deletefile(){
                $(".inputfile").val('');
                $(".inputfile").parent().find('span').html('UPLOAD');
                $(".filename").html("No File Chosen");
            }
              $('#removefile').on('click', function(){
                console.log('aa');
                $(".inputfile").val('');
              });
        </script>

    </body>
</html>