<header class="desktop" style="display:none;">
		<div class="nav-top navbar">
			<div class="container">
				<div class="logo_wrapper pull-left navbar-brand">
					<a href=""><img src="images/logo.png"></a>
				</div>
				<div class="navbar-right">
					<a href="#" class="btn btn-bgwhite" data-toggle="modal" data-target="#modal-login">login</a>
					<a href="#" class="btn btn-bggreen" data-toggle="modal" data-target="#modal-register">sign up</a>
				</div>
			</div>
		</div>
		<div class="nav-search">
			<div class="container">
				<div class="row">
					<div class="col col-md-9">
						<form>
							<div class="input-group">
						      <input type="text" class="form-control" placeholder="What are you looking for..." aria-label="Search for...">
						      <span class="input-group-btn">
						        <button class="btn btn-search" type="button"><i class="pe-7s-search"></i></button>
						      </span>
								<div class="dropdown show">
								  <a class="btn btn-categories dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    All Categories
								  </a>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
								    <a class="dropdown-item" href="#">Requested Item</a>
								  </div>
								</div>
						    </div>
						</form>
					</div>
					<div class="col col-md-3 contact-header">
						<div class="row">
							<div class="contact-left icon-hover_wrapper col-auto">
								<a href="#"><i class="pe-7s-headphones stroke"></i> <i class="pe-7f-headphones filled"></i></a>
							</div>
							<div class="contact-right">
								<div class="text">Support 24/7</div>
								<div class="icon-hover_wrapper">
									<a href="#"><i class="pe-7s-mail stroke"></i> <i class="pe-7f-mail filled"></i></a>-
									<a href="#"><i class="pe-7s-chat stroke"></i> <i class="pe-7f-chat filled"></i></a>-
									<a href="#"><i class="pe-7s-phone stroke"></i> <i class="pe-7f-phone filled" style="margin-left:2px;"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					</div>
			</div>
		</div>
</header>
<header class="mobile">
		<div class="nav-top navbar">
			<div class="row">
				<div class="menu-mobile col-3">
					<a class="btn btn-menu-mobile"><i class="pe-7s-menu"></i></a>
				</div>
				<div class="menu-content">
					<div class="navbar-right">
						<a href="#" class="btn btn-bgwhite" data-toggle="modal" data-target="#modal-login">login</a>
						<a href="#" class="btn btn-bggreen" data-toggle="modal" data-target="#modal-register">sign up</a>
					</div>
				</div>
				<div class="col-9">
					<a href=""><img src="images/logo.png" class="img-fluid"></a>
				</div>
			</div>
		</div>
		<div class="nav-search">
			<div class="container">
				<div class="row">
					<div class="col col-md-9">
						<form>
							<div class="input-group">
						      <input type="text" class="form-control" placeholder="What are you looking for..." aria-label="Search for...">
						      <span class="input-group-btn">
						        <button class="btn btn-search" type="button"><i class="pe-7s-search"></i></button>
						      </span>
								<div class="dropdown show">
								  <a class="btn btn-categories dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    All Categories
								  </a>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
								    <a class="dropdown-item" href="#">Requested Item</a>
								  </div>
								</div>
						    </div>
						</form>
					</div>
					<div class="col col-md-3 contact-header">
						<div class="row">
							<div class="contact-left icon-hover_wrapper col-auto">
								<a href="#"><i class="pe-7s-headphones stroke"></i> <i class="pe-7f-headphones filled"></i></a>
							</div>
							<div class="contact-right">
								<div class="text">Support 24/7</div>
								<div class="icon-hover_wrapper">
									<a href="#"><i class="pe-7s-mail stroke"></i> <i class="pe-7f-mail filled"></i></a>-
									<a href="#"><i class="pe-7s-chat stroke"></i> <i class="pe-7f-chat filled"></i></a>-
									<a href="#"><i class="pe-7s-phone stroke"></i> <i class="pe-7f-phone filled" style="margin-left:2px;"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					</div>
			</div>
		</div>
</header>


<!-- Modal LOGIN -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="title">LOGIN</div>
        <div class="content">
        	<form>
			  <div class="form-group">
			    <label for="exampleInputEmail1">E-Mail</label>
			    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-Mail">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>
			  <div class="form-check">
			    <label class="custom-control custom-checkbox">
			      <input type="checkbox" class="custom-control-input">
			      <span class="custom-control-indicator"></span>
			     Remember Me
			    </label>
			    <a href="#" class="forgot-link pull-right">
			   		Forgot Password ?
			    </a>
			  </div>
			  <button type="submit" class="btn btn-block btn-submit mt-3">LOGIN</button>
			  <div class="not-member mt-3">
			  	Not yet a member? <a href="#">Sign Up!</a>
			  </div>
			</form>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Modal REGISTER -->
<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="title">REGISTER A <span class="green">NEW ACCOUNT</span></div>
        <div class="content">
        	<form>
        	  <div class="form-group">
			    <label for="exampleInputEmail1">Full Name</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name">
			    <div class="alert alert-danger"><strong>Error!</strong> The name field is required.</div>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">E-mail</label>
			    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Postal Code</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Postal Code">
			  </div>
			   <div class="form-group">
			    <label for="exampleInputEmail1">MRT Station</label>
			    <select id="inputState" class="form-control">
			    	<option disabled selected>Select Station</option>
			    	<option>Bugis</option>
			    	<option>Rochor</option>
			    	<option>Little India</option>
			    	<option>Harbour Front</option>
			    	<option>Chinatown</option>
			    	<option>Orchard</option>
			    	<option>Somerset</option>
			    	<option>Marina Bay</option>
			    	<option>Expo</option>
			    </select>
			  </div>

			  <div class="form-group">
			    <label for="exampleInputPassword1">Mobile Number</label>
			     <div class="input-group input-mobile">
			        <div class="input-group-addon">+</div>
			        <input type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="">
			      </div>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Confirm Password</label>
			    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>

			  <div class="form-check form-agreement">
			    <label class="custom-control custom-checkbox">
			      <input type="checkbox" class="custom-control-input">
			      <span class="custom-control-indicator"></span>
			    	I agree to the Rent Tycoons' Terms of Use including the collection, use and disclosure of my personal data in accordance with and for the purposes set out in the Privacy Policy
			    </label>
			  </div>
			  <div class="captcha_wrapper">
			  	<div class="captcha_image">
			  		<img src="images/captcha.jpg">
			  	</div>
			  	<div class="captcha_reload mt-1">
			  		<a href="#"><img src="images/reload-captcha.png"></a>
			  	</div>
			  </div>
			  <div class="btn_wrapper mt-2">
			  	 <div class="form-row">
				    <div class="col-auto">
				      <label class="captcha-text" for="inlineFormInput">Type the text</label>
				    </div>
				    <div class="col">
				      <input type="text" class="form-control form-captcha" placeholder="">
				    </div>
				    <div class="col">
				     	<button type="submit" class="btn btn-submit btn-block">REGISTER</button>
				    </div>
				  </div>
  				  
			  </div>
			</form>
        </div>
      </div>
    </div>
  </div>
</div>