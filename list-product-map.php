<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
          <style type="text/css">
            /* Set a size for our map container, the Google Map will take up 100% of this container */
            #map {
                width: 750px;
                height: 500px;
            }
        </style>
        
    </head>
    <body>
        <?php include('header.php'); ?>

        <section class="section-listproduct section-listproduct-map">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Bedroom</li>
                    </ol>
                </div>

                <div class="main-content">
                    <div class="row list-product">
                        <div class="sidebar_wrapper col-md-3">
                            <div class="sidebar">
                                <nav class="nav flex-column">
                                  <a class="nav-link active" href="#">COOKER</a>
                                  <a class="nav-link" href="#">Cleaning</a>
                                  <a class="nav-link" href="#">Kitchen Tools</a>
                                  <a class="nav-link" href="#">Bowls</a>
                                  <a class="nav-link" href="#">Refrigrator</a>
                                  <a class="nav-link" href="#">Blender</a>
                                </nav>
                            </div>

                            
                        </div><!--END SIDEBAR-->
                        <div class="list_product_right col-md-9">
                            <div class="title mb-4">
                                Bedroom
                            </div>
                            <div class="filter-top">
                                <div class="row">
                                    <div class="col-md-3 sort-icon">
                                        <a href="#" class="filter-icon btn"><i class="pe-7f-keypad"></i></a>
                                        <a href="#" class="filter-icon btn"><i class="pe-7f-menu"></i></a>
                                        <a href="#" class="filter-icon btn"><i class="pe-7f-map-marker"></i></a>
                                    </div>
                                    <div class="col-md-5 sort-option">
                                         <div class="form-group">
                                            <label class="mr-2">Sort by</label>
                                            <select >
                                                <option disabled selected>--</option>
                                                <option>Name</option>
                                                <option>Price</option>
                                            </select>
                                            <label class="mr-2 ml-3">Show</label>
                                            <select class="select-show">
                                                <option>10</option>
                                                <option>15</option>
                                                <option>20</option>
                                            </select>
                                            <label class="ml-2">per page</label>
                                          </div>
                                    </div>
                                    <div class="col-md-4 sort-text text-right">
                                        There are 13 products
                                    </div>
                                </div>
                            </div>
                            <div class="list-product mt-4">
                                <div id="map"></div>
                            </div>
                            <div class="filter-bottom filter-sidebar mt-4 pl-5 pr-5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="categories_wrapper">
                                            <div class="title">CATEGORIES</div>
                                            <div class="content">
                                                <div class="form-check form-agreement">
                                                    <label class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input">
                                                      <span class="custom-control-indicator"></span>
                                                       Dining & Bar (13)
                                                    </label>
                                                </div>
                                                <div class="form-check form-agreement">
                                                    <label class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input">
                                                      <span class="custom-control-indicator"></span>
                                                       Office Furniture (13)
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="availability_wrapper">
                                            <div class="title">AVAILABILITY</div>
                                            <div class="content">
                                                <div class="form-check form-agreement">
                                                    <label class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input">
                                                      <span class="custom-control-indicator"></span>
                                                       Instock (13)
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="condition_wrapper">
                                            <div class="title">CONDITION</div>
                                            <div class="content">
                                                <div class="form-check form-agreement">
                                                    <label class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input">
                                                      <span class="custom-control-indicator"></span>
                                                       New (13)
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mrt_wrapper">
                                            <div class="title">MRT STATION</div>
                                            <div class="content">
                                                <select id="inputState" class="form-control">
                                                    <option disabled selected>Select Station</option>
                                                    <option>Bugis</option>
                                                    <option>Rochor</option>
                                                    <option>Little India</option>
                                                    <option>Harbour Front</option>
                                                    <option>Chinatown</option>
                                                    <option>Orchard</option>
                                                    <option>Somerset</option>
                                                    <option>Marina Bay</option>
                                                    <option>Expo</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="price_wrapper">
                                            <div class="title">PRICE</div>
                                            <div class="content price-range_wrapper">
                                                <label>Range: <span class="amount" id="amount">$14.00 - $53.00</span></label>
                                                <div id="slider-range" class="mt-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination_wrapper pagination-full mt-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination mr-auto ml-auto">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--END MAIN CONTENT-->
            </div>
        </section>
        <?php include('footer.php'); ?>


        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
         <script>
          $( function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [ 75, 300 ],
              slide: function( event, ui ) {
                $( "#amount" ).text( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            $( "#amount" ).text( "$" + $( "#slider-range" ).slider( "values", 0 ) +
              " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          } );
          </script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7P_yS37K6HYgbw5Feu5USpXvDgpNwJvc" type="text/javascript"></script>
        
        <script type="text/javascript">

            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(1.3143394,103.7041631), // Singapore

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(1.3042758,103.8529748),
                    map: map,
                    icon: 'images/icon/mapmarker.png'
                });
                var contentString = '<div class="product-box product-map"><div class="media"><div class="d-flex align-self-center mr-3 col-media"><img class="img-product img-fluid" src="images/list-product.png" alt="Generic placeholder image"></div> <div class="media-body"><div class="product-desc"><div class="product-name"><a href="#"> MINI DESK GRAY COLORED</a></div><div class="product-price"><span class="main-price">$72.00</span><span class="disc-price">$89.00</span></div><div class="product-rating"><i class="pe-7s-star active"></i><i class="pe-7s-star active"></i><i class="pe-7s-star active"></i><i class="pe-7s-star"></i><i class="pe-7s-star"></i> </div> </div></div> </div></div>';
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }

        
        </script>
    </body>
</html>