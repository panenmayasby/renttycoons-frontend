<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-termsofuse section-information">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Terms of Use</li>
                    </ol>
                </div>
                <div class="main-content main-information">
                    <div class="main-title">Terms of Use</div>
                    <div class="row information">
                        <div class="col-md-3 col-sm-4 nav-left_wrapper">
                            <nav class="nav flex-column nav-left">
                              <a class="nav-link active" href="#">Terms of Use</a>
                              <a class="nav-link" href="#">Sample Of Rental Agreement</a>
                              <a class="nav-link" href="#">Privacy Policy</a>
                              <a class="nav-link" href="#">Rental Policy</a>
                              <a class="nav-link" href="#">Prohibited Items</a>
                              <a class="nav-link" href="#">Media Releases</a>
                            </nav>
                        </div>
                        <div class="col-md-9 col-sm-8 info-right_wrapper">
                            <div class="opening-text">
                              <p>Thank you for your interest in Rent Tycoons. Rent Tycoons Pte. Ltd., a Singapore corporation (hereinafter, “Rent Tycoons”, or “we”, “our” or “us”) provides a collection of services and tools (hereinafter the “Services”), including without limitation providing an online presence for you to view, rent, or post “Rental Listings” and other Content (as defined below), subject to the following Terms of Use. These Terms of Use apply to any and all persons, companies, Members, or any other users (hereinafter "you", or "your") of the website (www.renttycoons.com), including any and all sub domains, and any and all replacement or successor websites, or portions thereof (hereinafter referred to as the “Site”). Any and all persons, companies or any other users who have successfully registered as a member of Rent Tycoons on the Site, will hereinafter be referred to as "Member". These Terms of Use must be read in conjunction with, and includes, all Rent Tycoons policies available on the Site (the “Policies”). You shall review the sample of the rental contract and the Policies through the following links:</p>
                              <p>
                                <ul>
                                  <li>
                                    <a href="#">Sample Of Rental Agreement</a>
                                  </li>
                                  <li>
                                    <a href="#">Privacy Policy</a>
                                  </li>
                                  <li>
                                    <a href="#">Rental Policy</a>
                                  </li>
                                  <li>
                                    <a href="#">Prohibited Items</a>
                                  </li>
                                </ul>
                              </p>
                              <p>BY USING THE SITE IN ANY WAY, YOU ARE ACCEPTING ALL TERMS OF USE AND ALL POLICIES, AND YOU SHALL COPMY IN ACCORDANCE WITH THEM. BEFORE YOU DECIDE TO BECOME A MEMBER, YOU MUST READ AND ACCEPT THE TERMS OF USE AND ALL POLICIES OF RENT TYCOONS</p>

                             <p>Should you object to any of the Terms of Use or Policies, or any subsequent changes or become dissatisfied with Rent Tycoons in any way, your only recourse is to immediately discontinue the use (of the Site of) of Rent Tycoons. Rent Tycoons has the right, but is not obligated, to strictly enforce the Terms of Use or Policies through self-help, community moderation, active investigation, litigation and prosecution.</p>
                            </div>
                            <div class="info-box_wrapper">
                                <div class="card info-box active">
                                      <div class="card-header">
                                        Disclaimers
                                      </div>
                                      <div class="card-body">
                                        <p>YOU AGREE AND ACCEPT THAT USING THE SITE AND THE SERVICE IS ENTIRELY AT YOUR OWN RISK AND YOU ASSUME TOTAL RESPONSIBILITY FOR THE USE OF THE SITE AND THE SERVICES AND ANY RENTAL TRANSACTION YOU ENTER INTO. YOUR SOLE REMEDY AGAINST RENT TYCOONS FOR DISSATISFACTION WITH THE SITE, THE SERVICES, OR ANY CONTENT, IS TO STOP USING THE SITE AND THE SERVICE. THE SITE AND THE SERVICE ARE PROVIDED ON AN "AS IS" OR "AS AVAILABLE" BASIS, WITHOUT ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND.</p>

                                        <p>Under no circumstances shall Rent Tycoons be liable for (DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES (EVEN IF RENT TYCOONS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), resulting from your use of the Site or the Service, or resulting from any rental transaction, including, but not limited to, damages or injury resulting from a rented item or non-returned item or non-payment by any other user, damages which arise from use or misuse of the Site or the Service, from inability to use the Site or the Service, or the interruption, suspension, modification, alteration, or termination of the Site, the Service or member account. Such limitation shall also apply with respect to damages incurred by reason of other Services or products received through or advertised in connection with the Site or the Service or any links on the Site, as well as by reason of any information or advice received through or advertised in connection with the Site or the Service or any links on the Site.</p>

                                        <p>RENT TYCOONS EXPRESSLY DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE WARRANTIES OF ACCURACY, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS. RENT TYCOONS DOES NOT REPRESENT OR WARRANT THAT THE SITE OR ANY CONTENTS, SERVICES, OR FEATURES WILL BE ERROR-FREE OR UNINTERRUPTEDED, THAT ANY DEFECTS WILL BE CORRECTED, THAT YOUR USE OF THE SITE WILL PROVIDE SPECIFIC RESULTS, OR THAT ANY FILES OR OTHER DATA YOU DOWNLOAD FROM THE SITE WILL BE FREE OF VIRUSES OR CONTAMINATION OR DESTRUCTIVE FEATURES.</p>

                                        <p>RENT TYCOONS DISCLAIMS ANY AND ALL LIABILITY FOR THE ACTS, OMMISSIONS AND CONDUCT OF ANY THIRD PARTIES, INCLUDING OTHER USERS, IN CONNECTION WITH OR RELATED TO YOUR USE OF THE SITE OR SERVICES, INCLUDING, BUT NOT LIMITED TO, ITEMS RENTED TO OR FROM YOU IN CONNECTION WITH YOUR USE OF THE SITE OR SERVICES, SERVICES OBTAINED OR GIVEN BY YOU IN CONNECTION WITH YOUR USE OF THE SITE OR SERVICES, AND/OR ANY OTHER RENT TYCOONS SERVICES.</p>

                                        <p>RENT TYCOONS SHALL NOT BE LIABLE FOR THE DELETION OR FAILURE TO STORE ANY CONTENT MAINTAINED OR TRANSMITTED BY THE SERVICE.</p>

                                        <p>RENT TYCOONS WILL NOT PROVIDE LEGAL ADVICE REGARDING ANY SERVICES YOU USE (OR PROPOSE TO USE). NO ACT OF RENT TYCOONS SHALL BE INTERPRETED AS PROVIDING LEGAL ADVICE, INCLUDING BUT NOT LIMITED TO PROVIDING LEGAL DOCUMENT TEMPLATES OR PERMITTING A RENTAL TRANSACTION TO OCCUR. RENT TYCOONS ENCOURAGES YOU TO SEEK YOUR OWN LEGAL COUNSEL IF YOU HAVE ANY QUESTIONS REGARDING YOUR PERSONAL LIABILITY FOR ANY PARTICULAR TRANSACTION OR IF YOU WOULD LIKE A RENTAL AGREEMENT DRAFTED AND/OR REVIEWED.</p>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                         Limitation of Liability
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        Ownership of Site and Content
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        Member Account Information
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        Member Account Access and Password
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                                <div class="card info-box">
                                      <div class="card-header">
                                        Termination of Member Account
                                      </div>
                                      <div class="card-body">
                                        <p>Rent Tycoons is an online marketplace that facilitates peer-to peer-renting in Singapore.</p>

                                        <p class="marginbottom5">Our portal enables you to do the following:</p>
                                        <ol>
                                            <li>Make money by putting your items/services up for rent.</li>
                                            <li>Save money by searching and renting items/services that you need.</li>
                                            <li>Submit a request for desired items/services that are not yet available on the portal.</li>
                                        </ol>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script>
            $(document).ready(function() {
                
            });
            $(document).on('click', '.card-header', function(){ 
               $(this).parent().find(".card-body").slideToggle();
               $(this).parent().toggleClass('active');
           });
        </script>
    </body>
</html>