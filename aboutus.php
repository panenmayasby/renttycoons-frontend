<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-aboutus">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">About Us</li>
                    </ol>
                </div>
                <div class="main-content mb-5">
                    <div class="row">
                        <div class="col-md-5 text-right img_wrapper left">
                            <img src="images/aboutus.png" class="img-fluid">
                        </div>
                        <div class="col-md-7 right">
                            <div class="title">
                                WHO WE ARE
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
                            </div>
                            <div class="title mt-4">
                                OUR SERVICES
                            </div>
                            <div class="content">
                                <div class="list-green">
                                    <ul>
                                        <li>Aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum 
exercitationemullam corporis suscipit laboriosam, nisi ut aliquid.</li>
                                        <li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis 
praesentium voluptatum deleniti atque corrupti quos dolores</li>
                                        <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit 
quo minus id quod maxime placeat facere</li>
                                        <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit 
quo minus id quod maxime placeat facere</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 left">
                            <div class="title">
                                WHAT WE CAN DO
                            </div>
                            <div class="content">
                                <div class="quotes">
                                     Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur.
                                </div> 
                                Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                        </div>
                        <div class="col-md-7 right">
                            <div class="title">
                                SKILLS
                            </div>
                            <div class="content">
                                <label>Investment Trusts</label>
                                <label class="percent" style="left:85%;">90%</label>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <label>Trades & Stocks </label>
                                <label class="percent" style="left:75%;">80%</label>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <label>Financial Planning</label>
                                <label class="percent" style="left:65%;">70%</label>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer-landing.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>