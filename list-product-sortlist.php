<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-listproduct section-sortlist">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active">Bedroom</li>
                    </ol>
                </div>

                <div class="main-content">
                    <div class="row list-product">
                        <div class="sidebar_wrapper col-md-3">
                            <div class="sidebar">
                                <nav class="nav flex-column">
                                  <a class="nav-link active" href="#">COOKER</a>
                                  <a class="nav-link" href="#">Cleaning</a>
                                  <a class="nav-link" href="#">Kitchen Tools</a>
                                  <a class="nav-link" href="#">Bowls</a>
                                  <a class="nav-link" href="#">Refrigrator</a>
                                  <a class="nav-link" href="#">Blender</a>
                                </nav>
                            </div>

                            <div class="filter-sidebar mt-5">
                                <div class="categories_wrapper">
                                    <div class="title">CATEGORIES</div>
                                    <div class="content">
                                        <div class="form-check form-agreement">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                               Dining & Bar (13)
                                            </label>
                                        </div>
                                        <div class="form-check form-agreement">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                               Office Furniture (13)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="availability_wrapper">
                                    <div class="title">AVAILABILITY</div>
                                    <div class="content">
                                        <div class="form-check form-agreement">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                               Instock (13)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="condition_wrapper">
                                    <div class="title">CONDITION</div>
                                    <div class="content">
                                        <div class="form-check form-agreement">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                               New (13)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="mrt_wrapper">
                                    <div class="title">MRT STATION</div>
                                    <div class="content">
                                        <select id="inputState" class="form-control">
                                            <option disabled selected>Select Station</option>
                                            <option>Bugis</option>
                                            <option>Rochor</option>
                                            <option>Little India</option>
                                            <option>Harbour Front</option>
                                            <option>Chinatown</option>
                                            <option>Orchard</option>
                                            <option>Somerset</option>
                                            <option>Marina Bay</option>
                                            <option>Expo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="price_wrapper">
                                    <div class="title">PRICE</div>
                                    <div class="content price-range_wrapper">
                                        <label>Range: <span class="amount" id="amount">$14.00 - $53.00</span></label>
                                        <div id="slider-range" class="mt-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--END SIDEBAR-->
                        <div class="list_product_right col-md-9">
                            <div class="title mb-4">
                                Bedroom
                            </div>
                            <div class="filter-top">
                                <div class="row">
                                    <div class="col-md-3 sort-icon">
                                        <a href="#" class="filter-icon btn"><i class="pe-7f-keypad"></i></a>
                                        <a href="#" class="filter-icon btn"><i class="pe-7f-menu"></i></a>
                                        <a href="#" class="filter-icon btn"><i class="pe-7f-map-marker"></i></a>
                                    </div>
                                    <div class="col-md-5 sort-option">
                                         <div class="form-group">
                                            <label class="mr-2">Sort by</label>
                                            <select >
                                                <option disabled selected>--</option>
                                                <option>Name</option>
                                                <option>Price</option>
                                            </select>
                                            <label class="mr-2 ml-3">Show</label>
                                            <select class="select-show">
                                                <option>10</option>
                                                <option>15</option>
                                                <option>20</option>
                                            </select>
                                            <label class="ml-2">per page</label>
                                          </div>
                                    </div>
                                    <div class="col-md-4 sort-text text-right">
                                        There are 13 products
                                    </div>
                                </div>
                            </div>
                            <div class="list-product mt-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="mr-3 col-media">
                                                    <div class="img-product_wrapper d-flex align-self-center">
                                                        <img class="img-product img-fluid" src="images/tes.jpg" alt="Generic placeholder image">
                                                    </div>
                                                    <div class="button_wrapper">
                                                        <a href="#" class="btn btn-bggreen btn-square btn-block">QUICK VIEW</a>
                                                    </div>
                                                    <div class="promote">
                                                        <i class="pe-7s-speaker"></i>
                                                    </div>
                                              </div>
                                              <div class="media-body">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="button_wrapper mt-3">
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-cart"></i>&nbsp;&nbsp;Add to Cart</a>
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-like"></i></a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="mr-3 col-media">
                                                    <div class="img-product_wrapper d-flex align-self-center ">
                                                        <img class="img-product img-fluid" src="images/news-banner.jpg" alt="Generic placeholder image">
                                                    </div>
                                                    <div class="button_wrapper">
                                                        <a href="#" class="btn btn-bggreen btn-square btn-block">QUICK VIEW</a>
                                                    </div>
                                                    <div class="promote">
                                                        <i class="pe-7s-speaker"></i>
                                                    </div>
                                              </div>
                                              <div class="media-body">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="button_wrapper mt-3">
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-cart"></i>&nbsp;&nbsp;Add to Cart</a>
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-like"></i></a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="mr-3 col-media">
                                                    <div class="img-product_wrapper d-flex align-self-center">
                                                        <img class="img-product img-fluid" src="images/tes.jpg" alt="Generic placeholder image">
                                                    </div>
                                                    <div class="button_wrapper">
                                                        <a href="#" class="btn btn-bggreen btn-square btn-block">QUICK VIEW</a>
                                                    </div>
                                                    <div class="promote">
                                                        <i class="pe-7s-speaker"></i>
                                                    </div>
                                              </div>
                                              <div class="media-body">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="button_wrapper mt-3">
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-cart"></i>&nbsp;&nbsp;Add to Cart</a>
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-like"></i></a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="d-flex align-self-center mr-3 col-media">
                                                    <img class="img-product img-fluid" src="images/list-product.png" alt="Generic placeholder image">
                                                    <div class="button_wrapper">
                                                        <a href="#" class="btn btn-bggreen btn-square btn-block">QUICK VIEW</a>
                                                    </div>
                                              </div>
                                              <div class="media-body">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="button_wrapper mt-3">
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-cart"></i>&nbsp;&nbsp;Add to Cart</a>
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-like"></i></a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                    <div class="col-md-12">
                                        <div class="product-box">
                                            <div class="media">
                                              <div class="d-flex align-self-center mr-3 col-media">
                                                    <img class="img-product img-fluid" src="images/list-product.png" alt="Generic placeholder image">
                                                    <div class="button_wrapper">
                                                        <a href="#" class="btn btn-bggreen btn-square btn-block">QUICK VIEW</a>
                                                    </div>
                                              </div>
                                              <div class="media-body">
                                                <div class="product-desc">
                                                    <div class="product-name">
                                                       <a href="#"> MINI DESK</a>
                                                    </div>
                                                    <div class="product-rating">
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star active"></i>
                                                        <i class="pe-7s-star"></i>
                                                        <i class="pe-7s-star"></i>
                                                    </div>
                                                    <div class="product-text mt-2 mb-3">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat.
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="main-price">$72.00</span>
                                                        <span class="disc-price">$89.00</span>
                                                    </div>
                                                    <div class="button_wrapper mt-3">
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-cart"></i>&nbsp;&nbsp;Add to Cart</a>
                                                        <a href="#" class="btn btn-addtocart mr-2"><i class="pe-7f-like"></i></a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div><!--END COL-->
                                </div>
                            </div>
                            <div class="pagination_wrapper pagination-full mt-4">
                                <div class="col-md-4 text">
                                    Showing 1 - 12 of 13 items
                                </div>
                                <div class="col-md-8 button-pagination mr-auto ml-auto">
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="pe-7s-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                          <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="pe-7s-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                        </li>
                                      </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--END MAIN CONTENT-->
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
         <script>
          $( function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [ 75, 300 ],
              slide: function( event, ui ) {
                $( "#amount" ).text( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            $( "#amount" ).text( "$" + $( "#slider-range" ).slider( "values", 0 ) +
              " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          } );
          </script>
    </body>
</html>