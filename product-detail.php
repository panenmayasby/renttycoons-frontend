<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Rent Tycoon</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <!-- Optional - Adds useful class to manipulate icon font display -->
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <section class="section-productdetail">
            <div class="container">
                <div class="breadcrumb_wrapper">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item"><a href="#">Bedroom</a></li>
                      <li class="breadcrumb-item active">Printed Dress</li>
                    </ol>
                </div>

                <div class="main-content">
                    <div class="row detail-product">
                        <div class="sidebar_wrapper col-md-3">
                            <div class="sidebar">
                                <nav class="nav flex-column">
                                  <a class="nav-link active" href="#">COOKER</a>
                                  <a class="nav-link" href="#">Cleaning</a>
                                  <a class="nav-link" href="#">Kitchen Tools</a>
                                  <a class="nav-link" href="#">Bowls</a>
                                  <a class="nav-link" href="#">Refrigrator</a>
                                  <a class="nav-link" href="#">Blender</a>
                                </nav>
                            </div>
                        </div><!--END SIDEBAR-->
                    </div>
                </div><!--END MAIN CONTENT-->
            </div>
        </section>
        <?php include('footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/popper.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>

    </body>
</html>